# SimonHERE

an indicator if i am in the office.

## Terminal

Configuration and Monitoring Utility, see [terminal/](terminal/)

![Terminal Screenshot](documentation/terminal_status_early_build.png)

## Hardware

### Sniffer

- ESP32 Wemos D1 -> the brain
- [3DR Modem](documentation/Si1000.pdf) -> 433MHz relay to display

![WEMOS D1 Pins](documentation/wemos_d1_mini_esp32_pinout.jpg)

### Display

- [Digispark](https://digistump.com/wiki/digispark/tutorials/connecting) ATTiny85 -> the brain (not much to be honest)
- [3DR Modem](documentation/Si1000.pdf) -> 433MHz Relay to Sniffer
- Generic Servo -> Displays State

![DigiSpark pins](documentation/digispark_attiny85_pinout_by_mortenaaserud_dc7h4n3-pre.jpg)
