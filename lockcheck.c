#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/extensions/scrnsaver.h>
// requires package libx11-dev libxss-dev


/**
 * Report amount of X server idle time.
 * 
 * Build with- */
 * gcc -o lockcheck -lX11 -lXext -lXss lockcheck.c
 */


int main(int argc, char *argv[])
{
    Display *display;
    int event_base, error_base;
    XScreenSaverInfo info;
    float seconds;

    display = XOpenDisplay("");

    if (XScreenSaverQueryExtension(display, &event_base, &error_base)) {
        XScreenSaverQueryInfo(display, DefaultRootWindow(display), &info);

        seconds = (float)info.idle/1000.0f;
        printf("%f, state: %d {Off,On,Disabled}\n", seconds, info.state);
        return(0);
    } else {
        fprintf(stderr,"Error: XScreenSaver Extension not present\n");
        return(1);
    }
}
