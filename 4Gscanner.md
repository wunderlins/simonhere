# Mobile Scanner

## Software

- [Airprobe](http://git.gnumonks.org/airprobe/)
- [IMSI-catcher](https://github.com/Oros42/IMSI-catcher)
- [kalibrate-hackrf](https://github.com/scateu/kalibrate-hackrf), [Tutorial] (https://www.blackhillsinfosec.com/intro-to-software-defined-radio-and-gsm-lte/)

### IMSI-Catcher install

    git clone https://github.com/Oros42/IMSI-catcher.git
	sudo apt install python3-numpy python3-scipy python3-scapy gr-gsm
		


### LTE Scanner Install

    sudo apt install librtlsdr-dev libblas-dev \
		     libitpp-dev gnuradio-dev libhackrf-dev
	git clone https://github.com/JiaoXianjun/LTE-Cell-Scanner.git
	cd LTE-Cell-Scanner/cmake/Modules		

On Debian: edit `cmake/Modules/FindHACKRF.cmake` and add: 

- FIND_PATH: `/usr/include/libhackrf` 
- FIND_LIBRARY: `/usr/lib/x86_64-linux-gnu`
-  

    cd ../..
    mkdir build_hackrf
    cd build_hackrf
    cmake ../ -DUSE_HACKRF=1
    make

### Airprobe install

    sudo apt-get –y install git-core autoconf automake libtool \
		     g++ python-dev swig libpcap0.8-dev
    apt install gr-gsm rtl-sdr libosmocore

    # clone airprobe, patched version
    git clone https://github.com/pcabreracamara/airprobe.git

    cd airprobe/gsm-receiver/
    ./bootstrap
    ./configure
    make
    cd ../..

    cd airprobe/gsmdecode/
    ./bootstrap
    ./configure
    make
    cd ../..

    sudo pip install gsm

    # test installation
    cd airprobe/gsm-receiver/src/python
    git clone https://github.com/hainn8x/capture_941.8M_112.cfile.git

    # start wireshark and test
    sudo wireshark &
    # add filter: gsmtap
    ./go.sh capture_941.8M_112.cfile/capture_941.8M_112.cfile 

## Tutorials

- [RTL-SDR Installation Guide for Linux](https://www.rtl-sdr.com/tag/install-guide/) - this is helpful, kernel module `dvb_usb_rtl28xxu` collides with rtl-sdr. Test with `rtl_test`
- [RTL-SDR TUTORIAL: ANALYZING GSM WITH AIRPROBE/GR-GSM AND WIRESHARK](https://www.rtl-sdr.com/rtl-sdr-tutorial-analyzing-gsm-with-airprobe-and-wireshark/)
- [IMSI-catcher](https://hydrasky.com/internet-of-things/scan-radio-signal-with-rtl-sdr/)

## Frequency Bands

[Mobilfunkfrequenzen in der Schweiz][8]

### GSM

| band | type       |    uplink |  downlink | channels        |
| ---: | :--------- | --------: | --------: | --------------- |
|  900 | [E-GSM][1] |   880-915 |   925-960 | 0-124, 975-1023 |
| 1800 | [DCS][2]   | 1710-1785 | 1805-1880 | 512-885         |


### UMTS

| band | type         |    uplink |  downlink | channels        |
| ---: | :----------- | --------: | --------: | --------------- |
|   B1 | [2100][3]    | 2110-2170 | 1805-1880 |
|   B8 | [900 GSM][4] |   880-915 |   925-960 | 0-124, 975-1023 |

### LTE

| band | type        |    uplink |  downlink | Bandbreite  |
| ---: | :---------- | --------: | --------: | ----------- |
|   B3 | [1800+][5]  | 1920-1980 | 2110-2170 |
|   B7 | [2600][6]   | 2500-2570 | 2620-2690 |
|  B20 | [800 DD][7] |   791-821 |   832-862 | 10Mhz/10MHz |


[1]: https://www.frequencycheck.com/bands/gsm-gprs-edge-band-10-e-gsm-900
[2]: https://www.frequencycheck.com/bands/gsm-gprs-edge-band-13-dcs-1800
[3]: https://www.frequencycheck.com/bands/umts-hspa-band-1-2100
[4]: https://www.frequencycheck.com/bands/umts-hspa-band-8-900-gsm
[5]: https://www.frequencycheck.com/bands/lte-band-3-1800
[6]: https://www.frequencycheck.com/bands/lte-band-7-2600
[7]: https://www.frequencycheck.com/bands/lte-band-20-800-dd
[8]: https://de.wikipedia.org/wiki/Mobilfunkfrequenzen_in_der_Schweiz