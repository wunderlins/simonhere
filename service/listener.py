#!/usr/bin/env python3

import sys
import serial
import time
import array
# https://stackoverflow.com/questions/52510375/screensaver-status-on-linux-via-python-dbus-using-python3
import dbus

baud = 115200
port = "/dev/airspycnc"
portopen = None
last_status = None

def now():
    return str(int(time.time()))

def send(command):
    global portopen
    b = bytearray()
    b.extend(map(ord, command))
    portopen.write(b)
    

def main():
    global baud, port, portopen, last_status
    
    session_bus = dbus.SessionBus()
    screensaver_list = ['org.freedesktop.ScreenSaver']

    while not portopen:
        portopen = serial.Serial(port, baud, timeout=1)
        print(now() + " Monitor: Opened " + port + '\r')
        
        portopen.flushInput()
        
        bytesToRead = 0;
        while True:
            
            # check if we need to sync the clock
            try:
                tdata = portopen.read()           # Wait forever for anything
                time.sleep(1)              # Sleep (or inWaiting() doesn't give the correct value)
                data_left = portopen.inWaiting()  # Get the number of characters ready to be read
                tdata += portopen.read(data_left) # Do the read and combine it with the first character
                line = tdata.decode().strip()
                
                if line != "":
                    if line[0] == "\r":
                        line = line[1:]
                    if line[-1] == "\r":
                        line = line + "\n"
                    print(now() + " AirSPY: " + line)
                    
                    # send current time as unix tst
                    if line == "time sync" or line == "sync":
                        tst = int(time.time())
                        tst_str = "time " + str(tst) + "\n"
                        send(tst_str)
                        
                        if line == "sync":
                            time.sleep(2)
                            
                            # send update to airspy
                            s = 0;
                            if status == True:
                                s = 1
                            cmd = "screenlock " + str(s) + "\n"
                            send(cmd)                          
                
            except IOError:
                # Manually raise the error again so it can be caught outside of this method
                raise IOError()
        
            # check for changes in screensvaer state
            for each in screensaver_list:
                try:
                    object_path = '/{0}'.format(each.replace('.', '/'))
                    get_object = session_bus.get_object(each, object_path)
                    get_interface = dbus.Interface(get_object, each)
                    status = bool(get_interface.GetActive())

                    # send status of screensve if it has changed
                    # or if we synced time in this cycle. This will make sure the 
                    # the tst_screenlock on the AirSPY  side will have an up to date
                    # timestamp
                    if last_status != status:
                        print(now()+ " screenlock: " + str(status))
                        
                        # send update to airspy
                        s = 0;
                        if status == True:
                            s = 1
                        cmd = "screenlock " + str(s) + "\n"
                        send(cmd)

                    last_status = status

                except dbus.exceptions.DBusException:
                    pass
        
        portopen.close()
        portopen = None


if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            # quit
            sys.exit()
        except:
            portopen.close()
            portopen = None
            print(now() + " Lost connection")
            time.sleep(1)
