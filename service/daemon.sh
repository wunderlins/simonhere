#!/usr/bin/env bash

# NOTE: to make this work, install the following udev rule:
#       99_airspycnc.rules
#       KERNEL=="rfcomm9", SYMLINK+="airspycnc"
#
# Set the mac address to devices
mac=BC:DD:C2:DF:8C:EA
dev=rfcomm9

function start() {
    rfcomm bind 9 $mac
}

function stop() {
    rfcomm release $(readlink -f /dev/airspycnc | cut -d'm' -f3)
}

function status() {
    rfcomm -a | grep $dev
}

case "$1" in
    start)
        start
        ;;
        
    stop)
        stop
        ;;
        
    status)
        status anacron
        ;;
    restart)
        stop
        sleep 1
        start
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart|condrestart|status}"
        exit 1
esac
