#!/usr/bin/env python3

# https://stackoverflow.com/questions/52510375/screensaver-status-on-linux-via-python-dbus-using-python3

import dbus
import time

last_status = None

session_bus = dbus.SessionBus()
screensaver_list = ['org.gnome.ScreenSaver',
                    'org.cinnamon.ScreenSaver',
                    'org.kde.screensaver',
                    'org.freedesktop.ScreenSaver']

screensaver_list = ['org.freedesktop.ScreenSaver']

if __name__ == "__main__":
    while True:
        for each in screensaver_list:
            try:
                object_path = '/{0}'.format(each.replace('.', '/'))
                get_object = session_bus.get_object(each, object_path)
                get_interface = dbus.Interface(get_object, each)
                status = bool(get_interface.GetActive())

                if last_status != status or last_status == None:
                    print(str(time.time()) + " " + str(status))

                last_status = status

            except dbus.exceptions.DBusException:
                pass
            
        time.sleep(1)
