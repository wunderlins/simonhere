#!/usr/bin/env bash

export PATH=$PATH:$(pwd)

alias spyterm='putty -serial /dev/airspycnc -fn "9x15" -title AirSPY &'
