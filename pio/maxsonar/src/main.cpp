/**
 * Sonar Example, MB1240
 * 
 * 2020-03-05, Simon Wunderlin
 */
#include "mp1240.h"

double range = 0;

void setup() {
	Serial.begin(9600);
	mp1240_setup();
}

void loop() {
#ifdef PWM
	range = pwm_read_sensor();
#else
	range = analog_read_sensor();
#endif
	Serial.print(range);
	Serial.println("cm");
	delay(100);
}
