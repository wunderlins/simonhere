/**
 * Sonar Example, MB1240
 * 
 * This is example code for the MaxBotix Sonar Sensor XL-MaxSOnar AE0.
 * Tested with ESP32, WEMOS D1 mini
 * 
 * https://www.maxbotix.com/Arduino-Ultrasonic-Sensors-085/
 * 
 * 
 * ELECTRICAL
 * - 5V - 3.2V
 * - Sonsor is output temperature compensated, max 40°C
 * 
 * 
 * PIN LAYOUT
 * 
 * - 7: GND - Return for the DC power supply.  GND (& V+) must be ripple 
 *      and noise free for best operation.
 * 
 * - 6: Vcc - Operates on 3.3V - 5V.  The average (and peak) current draw for 
 *      3.3V operation is 2.1mA (50mA peak) and at 5V operation is 3.4mA 
 *      (100mA peak) respectively. Peak current is used during sonar pulse 
 *      transmit. Please reference page 4 for minimum operating voltage
 *      verses temperature information.
 * 
 * - 5: TX - When Pin 1 is open or held high, the Pin 5 output delivers 
 *      asynchronous serial with an RS232 format, except voltages are 0-Vcc. 
 *      The output is an ASCII capital “R”, followed by three ASCII character 
 *      digits representing the range in centimeters up to a maximum of 765,
 *      followed by a carriage return (ASCII 13).  The baud rate is 9600, 
 *      8 bits, no parity, with one stop bit. Although the voltage of 0-Vcc 
 *      is outside the RS232 standard, most RS232 devices have sufficient 
 *      margin to read 0-Vcc serial data.  If standard voltage level RS232 is 
 *      desired, invert, and connect an RS232 converter such as a MAX232.
 * 
 *      When Pin 1 is held low, the Pin 5 output sends a single pulse, 
 *      suitable for low noise chaining (no serial data).
 * 
 * - 4: RX – This pin is internally pulled high.  The XL-MaxSonar-EZ sensors 
 *      will continually measure range and output if the pin is left 
 *      unconnected or held high.  If held low the will stop ranging.
 *      Bring high 20uS or more for range reading.
 * 
 * - 3: AN – For the 7.6 meter sensors (all sensors except for MB1260, 
 *      MB1261, MB1360, and MB1361), this pin outputs analog voltage with a 
 *      scaling factor of  (Vcc/1024) per cm.  A supply of 5V yields ~4.9mV/cm.,
 *      and 3.3V yields ~3.2mV/cm. Hardware limits the maximum reported range 
 *      on this output to ~700cm at 5V and ~600cm at 3.3V. The output is 
 *      buffered and corresponds to the most recent range data.
 * 
 *      For the 10 meter sensors (MB1260, MB1261, MB1360, MB1361), this pin 
 *      outputs analog voltage with a scaling factor of (Vcc/1024) per 2 cm.
 *      A supply of 5V yields ~4.9mV/2cm., and 3.3V yields ~3.2mV/2cm. The 
 *      output is buffered and corresponds to the most recent range data.
 * 
 * - 2: PW – For the MB1200 (EZ) sensor series, this pin outputs a pulse width 
 *      representation of range. To calculate distance, use the scale factor 
 *      of 58uS per cm. 
 * 
 *      For the MB1300 (AE) sensor series, this pin outputs the analog voltage 
 *      envelope of the acoustic wave form.  The output allows the user to 
 *      process the raw waveform of the sensor.
 * 
 * - 1: BW - Leave open (or high) for serial output on the Pin 5 output.
 *      When Pin 1 is held low the Pin 5output sends a pulse (instead of 
 *      serial data), suitable for low noise chaining.
 * 
 * 
 * TIMING DESCRIPTION
 * 
 * 175mS after power-up, the XL-MaxSonar is ready to begin ranging.  If Pin-4 
 * is left open or held high (20uS or greater), the sensor will take a range 
 * reading.  The XL-MaxSonar checks the Pin-4 at the end of every cycle. Range 
 * data can be acquired once every 99mS.  Each 99mS period starts by Pin-4 
 * being high or open, after which the XL-MaxSonar calibrates and calculates 
 * for 20.5mS, and after which, twenty 42KHz waves are sent.
 * 
 * At this point, for the MB1260, the pulse width (PW) Pin-2 is set high and 
 * until an object is detected after which the pin is set low. If no target 
 * is detected the PW pin will be held high for up to 44.4mS1
 * (i.e. 58uS * 765cm) or 62.0mS2(i.e. 58uS * 1068cm).   (For the most accurate
 * range data, use the PW output.)
 * 
 * For the MB1300 sensor series, The analog envelope output, Pin-2, will show 
 * the real-time signal return information of the analog waveform.
 * 
 * For both parts, the remainder of the 99mS time (less 4.7mS) is spent 
 * adjusting the analog voltage to the correct level, (and allowing the high 
 * acoustic power to dissipate).  During the last 4.7mS, the serial data 
 * is sent.
 * 
 * 2020-03-05, Simon Wunderlin
 */
#include "mp1240.h"

/**
 * Analog Read (Arduino, 5V, 10bit ADC)
 * 
 * The Arduino has a 10-bit ADC that it uses to read analog voltage signals. 
 * The MB1013 outputs a scale of 5mm per bit when reading Pin 3. This means 
 * every bit read by the Arduino has to be multiplied by 5 for the range in 
 * mm. To code this, use the example below. For the ultrasonic sensor 
 * scaling of a different ultrasonic sensor line please consult the 
 * sensor’s datasheet.
 */
double analog_read_sensor() {
	mp1240_raw = analogRead(mp1240_sonar_pin);
	// might need to scale for ESP voltage and ADC resolution
	mp1240_mm = mp1240_raw * 5; 
	return (double) mp1240_mm;
}

double pwm_read_sensor () {
	mp1240_raw = pulseIn(mp1240_sonar_pin, HIGH);
	
	// readings above 44400 uS are out of range readings
	// readings below  1160 uS are too close or 0 distance
	if (mp1240_raw > 44000 || mp1240_raw < 1160) {
		mp1240_mm = 0;
		return 0.0;
	}
	
	// 58 uS is equivalent to 1 mm
	mp1240_mm = mp1240_raw / 58; 
	
	return mp1240_mm;
}

void print_range() {
	Serial.print(mp1240_mm);
	Serial.print("cm, ");
	Serial.print(mp1240_raw);
	Serial.println(" raw");
}

void mp1240_setup() {
#ifdef PWM
	// enable for pwm mode
	pinMode(mp1240_sonar_pin, INPUT);
#endif
}
