#!/usr/bin/env bash

function init() {
	pio run
}

function monitor() {
	pio device monitor -b 9600
}

function build() {
	pio run -e wemos_d1_mini32
}

function upload() {
	pio run -e wemos_d1_mini32 -t upload
}
