# Linux Bluetooth Commands

**NOTE:** make sure your user is in the `dialout` group.

## general rfcomm notes
```bash
$ bluetoothctl list # my device
Controller 74:C6:3B:60:33:9A workshop2 [default]
...
$ bluetoothctl devices # paired devices
Device BC:DD:C2:DF:8C:EA AirspyComm
...
$ bluetoothctl scan on
Discovery started
[CHG] Controller 74:C6:3B:60:33:9A Discovering: yes
[NEW] Device 27:6C:75:50:F4:C8 27-6C-75-50-F4-C8
[NEW] Device C8:69:CD:0E:EC:2A C8-69-CD-0E-EC-2A
[NEW] Device 62:5A:A7:90:09:28 62-5A-A7-90-09-28
[NEW] Device 5D:4E:65:34:E6:C2 5D-4E-65-34-E6-C2
[CHG] Device BC:DD:C2:DF:8C:EA RSSI: -50
...
$ rfcomm connect /dev/rfcomm0 BC:DD:C2:DF:8C:EA& # create device
...
$ rfcomm -a # list open connections
rfcomm0: BC:DD:C2:DF:8C:EA channel 1 clean
```

## start/stop rfcomm device 

```bash
$ mac=BC:DD:C2:DF:8C:EA
# shut down all exiting connections
$ pids=$(ps aux | grep "/dev/rfcomm" | grep -v grep | awk '{print $2}')
$ sudo kill -9 $pids 
# for good measure
$ sudo rfcomm release 0 # where 0 is /dev/rfcommN
$ sudo rfcomm bind 0 $mac
```

## udev setup

### 99_airspycnc.rules

```
KERNEL=="rfcomm9", SYMLINK+="airspycnc"
```

### test symlink
```bash
# install rules
$ sudo cp 99_airspycnc.rules /etc/udev/rules.d
$ sudo udevadm control --reload-rules && sudo udevadm trigger

# create dev with link
$ mac=BC:DD:C2:DF:8C:EA
$ sudo rfcomm bind 9 $mac

# check
$ ls -la /dev/airspycnc 
lrwxrwxrwx 1 root root 7 Oct  8 09:30 /dev/airspycnc -> rfcomm9

# connect
$ screen /dev/airspycnc
# CTRL-A :quit

# remove 
$ sudo rfcomm release $(readlink -f /dev/airspycnc | cut -d'm' -f3)
```

# screensaver checks

## Possible dbus entries

```bash
$ qdbus org.freedesktop.ScreenSaver /ScreenSaver org.freedesktop.ScreenSaver.GetActive
$ qdbus org.kde.screensaver /ScreenSaver org.freedesktop.ScreenSaver.GetActive
$ qdbus org.gnome.ScreenSaver /ScreenSaver org.gnome.ScreenSaver.GetActive
```

## check for kde

```bash
while true; do 
    qdbus org.freedesktop.ScreenSaver \
        /ScreenSaver org.freedesktop.ScreenSaver.GetActive \
        >> sc.log;
    sleep 1; 
done
```
