#ifndef STATE_H
#define STATE_H

#include <stdint.h>
#include "console.h"

struct state_u {
    time_t tst_wifi;
    time_t tst_bt;
    time_t tst_screenlock;
    time_t tst_face;
    time_t tst_timesync;
    
    uint8_t wifi;
    uint8_t bt;
    uint8_t screenlock;
    uint8_t face;

    bool is_present;
};

typedef state_u state_t;

extern state_t state;

void cmd_screenlock(cmd_t* cmd);
void cmd_state(cmd_t* cmd);
void recalculate_state();
void setup_state();

#endif // STATE_H