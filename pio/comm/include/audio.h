#ifndef AUDIO_H
#define AUDIO_H

#include "Arduino.h"
#include "console.h"
#include "samples.h"

typedef enum AUDIO_FILES audio_files_t;
extern audio_files_t audio_files;

/**
 * Pins esp32 <-> MAX98357A 
 * 
 * 26 <-> BLCK
 * 25 <-> LRC
 * 22 <-> DIN
 * 
 * NOTE: provide minimum 650mA (better 850mA) to MAX98357A!
 *       I2S pin layout: https://diyi0t.com/i2s-sound-tutorial-for-esp32/
 */
#define I2S_BLCK 26
#define I2S_LRC  25
#define I2S_DOUT 22

// dependencies
#include <SD.h>
#include <HTTPClient.h>
#include "SPIFFS.h"

// audio libs https://github.com/earlephilhower/ESP8266Audio
#include "AudioFileSourcePROGMEM.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"

void setup_audio();
void play(audio_files_t index);

/**
 * play audio file
 * 
 * Audio files are stored in `samples_data.h` and are accessed 
 * by their index. The maximum file number is define also in 
 * `samples_data.h` with `const int audio_files_len`.
 * 
 * `cmd` may contain at least one up to N file indexes.
 */
void cmd_play(cmd_t* cmd);

#endif // AUDIO_H