#ifndef MAIN_H
#define MAIN_H

#include "Arduino.h"

// debug output over USB
#define DEBUGGING 0
// 0 = off, 1 = audio feedback on state changes
#define DEBUGGING_AUDIO 0
// enables bluetooth connection on startup
#define BT_ENABLED 1
// wifi scanner status pin definition
#define STATUS_PIN 18

// output stream, input can be Serial (USB) 
// or SerialBT (bluetooth serial)
extern Stream* output ;

#endif // MAIN_H