#ifndef DATETIME_H
#define DATETIME_H

#include "console.h"
#include "main.h"
#include <sys/time.h>

#define TIME_SYNC_TIMEOUT (60*60*2) // 2 hours in seconds

void cmd_time(cmd_t* cmd);
void sync_task();

#endif // DATETIME_H