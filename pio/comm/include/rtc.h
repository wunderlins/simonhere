/**
 * Real Time Clock
 * 
 * @see: https://www.sparkfun.com/products/10160
 * @see: https://learn.sparkfun.com/tutorials/deadon-rtc-breakout-hookup-guide
 * @see: https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/spi_master.html
 */

#ifndef RTC_H
#define RTC_H

#include <stddef.h>
#include "freertos/FreeRTOS.h"
#include "driver/spi_master.h"

#include <SPI.h>
#include "RtcDS3234.h"
/**
 * Reference for connecting SPI see https://www.arduino.cc/en/Reference/SPI
 * 
 * From RtcDS3234.h
 * 
 * CONNECTIONS:
 * - DS3234 MISO --> MISO
 * - DS3234 MOSI --> MOSI
 * - DS3234 CLK  --> CLK (SCK)
 * - DS3234 CS (SS) --> 5 (pin used to select the DS3234 on the SPI)
 * - DS3234 VCC --> 3.3v or 5v
 * - DS3234 GND --> GND
 * - SQW --->  (Pin19) Don't forget to pullup (4.7k to 10k to VCC)
 */

/**
 * ESP 32 pin configuration
 * 
 * Pins in use. The SPI Master can use the GPIO mux, 
 * so feel free to change these if needed.
 */
#define GPIO_HANDSHAKE 2
#define GPIO_MOSI 12
#define GPIO_MISO 13
#define GPIO_CS   14
#define GPIO_SCLK 15

//#ifdef CONFIG_IDF_TARGET_ESP32
#define SENDER_HOST HSPI_HOST
#define DMA_CHAN    2

/*
#elif defined CONFIG_IDF_TARGET_ESP32S2
#define SENDER_HOST SPI2_HOST
#define DMA_CHAN    SENDER_HOST
#endif
*/

void setup_rtc();
void loop_rtc();

#endif // RTC_H