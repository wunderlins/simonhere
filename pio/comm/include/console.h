#ifndef CONSOLE_H
#define CONSOLE_H

#include "Arduino.h"
#define CMD_MAX_ARGS 20

extern const char* commands[];

struct cmd {
    char *arg[CMD_MAX_ARGS];
    int length;
};
typedef struct cmd cmd_t;

enum command {
    CMD_INVALID = -2,
    CMD_UNKOWN = -1,
    CMD_HELP,
    CMD_SAY,
    CMD_STATUS,
    CMD_TIME,
    CMD_SCREENLOCK
};
typedef enum command command_t;


void console_prompt(Stream* device);
void setup_console();

/**
 * check if cmd string is a valid command
 * 
 * returns the index (starting at 0) of the valid command list
 * returns -1 for invalid command
 * 
 * @see commands
 */
command_t cmd_valid(cmd_t* cmd);

cmd_t* cmd_new(char* line, int length);
void cmd_free(cmd_t* cmd);

// base commands
void cmd_invalid(cmd_t* cmd);
void cmd_unknown(cmd_t* cmd);
void cmd_help(cmd_t* cmd);

#endif // CONSOLE_H