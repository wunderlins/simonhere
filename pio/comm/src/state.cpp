#include "main.h"
#include "audio.h"
#include "state.h"

state_t state = {0};

void recalculate_state() {
    // remember old state
    bool old_state = state.is_present;

    // if one of these is active, simon is here
    if (state.wifi || state.screenlock)
        state.is_present = true;
    else
        state.is_present = false;
    
    // notify
    if (old_state != state.is_present) {
        //Serial.printf("State change: %d\n", state.is_present);
        // TODO: send state change to modem

        if (state.is_present) {
            audio_files_t bye[3] = {HELLO, HELLO_MASTER, HELLO_MASTER_SIMON};
            int r = random(0, 3);
            play(bye[r]);
        } else {
            audio_files_t bye[3] = {GOOD_BYE, NICE_DAY, TIME_4_CURRY};
            int r = random(0, 3);
            Serial.printf("random 0-2: %d\n", r);
            play(bye[r]);
        }
    }
}

void setup_state() {
    ;
}

void cmd_screenlock(cmd_t* cmd) {
    if (cmd->length != 2) {
        output->println("Invalid number of arguments.");
        return;
    }

    int st = atoi(cmd->arg[1]);

    // reverse state, we want to know if i am present (lock == 0 means present)
    state.screenlock = (st == 1) ? 0 : 1;

    if (state.screenlock == 1) {
        time_t current_time = time(NULL);
        state.tst_screenlock = current_time;
    } else {
        state.tst_screenlock = 0;
    }

#if DEBUGGING_AUDIO > 0    
    // give feedback
    if (state.screenlock == 1)
        play(SC_UNLOCK);
    else
        play(SC_LOCK);
#endif

    recalculate_state();

}

void cmd_state(cmd_t* cmd) {
    output->printf("wifi:       %d %ld\n", state.wifi, state.tst_wifi);
    output->printf("bt:         %d %ld\n", state.bt, state.tst_bt);
    output->printf("screenlock: %d %ld\n", state.screenlock, state.tst_screenlock);
    output->printf("face:       %d %ld\n", state.face, state.tst_face);
    output->printf("timesync:      %ld\n", state.tst_timesync);
    output->printf("is_present: %d\n",     state.is_present);
}