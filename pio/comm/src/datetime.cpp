#include "datetime.h"
#include "audio.h"
#include "state.h"
#include "BluetoothSerial.h"

static TaskHandle_t task_time_sync_handle = NULL;
extern BluetoothSerial SerialBT;

void cmd_time(cmd_t* cmd) {
    // we accept 2 formats
    // time - output current systen time as unix timestamp
    // time NNNNN - set time from unix timestamp

    if (cmd->length == 1) {
        //time_t now;
        //char strftime_buf[64];
        //struct tm timeinfo;

        //time(&now);
        //getLocalTime(&now,0);
        //output->println(&now," %B %d %Y %H:%M:%S (%A)");


        char buff[20];
        time_t now = time(NULL);
        strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&now));
        output->print("Local time: ");
        output->println(buff);

        return;
    } 

    if (cmd->length == 2) {
        // convert str tst to time_t
        //const char* timestr = "1464478647000";
        time_t timenum = (time_t) strtol(cmd->arg[1], NULL, 10); 

        // set time
        struct timeval newtime = { .tv_sec = timenum }; 
        settimeofday(&newtime, NULL);

        state.tst_timesync = timenum;

        // give feedback
#if DEBUGGING_AUDIO > 0    
        play(TIME_SYNC);
#endif

        return;
    }

    output->println("Invalid arguments");
}

static void task_time_sync(void* parameter) {

    while (true) {
        //current_time = time(NULL);
        time_t current_time = time(NULL);
        struct tm* timeinfo = localtime(&current_time);
        //Serial.println(timeinfo->tm_year);
        
        // initia sync
        // - clock has not been set and year ist 1970 or
        // - last sync has been more than TIME_SYNC_TIMEOUT
        if (timeinfo->tm_year == 70) {
            // send sync command to pc
            SerialBT.println("sync");
        }
        
        // sporadic time sync
        if ((current_time - state.tst_timesync) > TIME_SYNC_TIMEOUT) {
            // send time sync command to pc
            SerialBT.println("time sync");
        }
        
        // wait ten seconds
        vTaskDelay(1e4 / portTICK_PERIOD_MS);
    }
}

void sync_task() {
    xTaskCreatePinnedToCore(
        task_time_sync,   /* Task function. */
        "task_sync",      /* String with name of task. */
        10000,            /* Stack size in bytes. */
        NULL,             /* Parameter passed as input of the task */
        1,                /* Priority of the task. */
        &task_time_sync_handle, /* Task handle. */
        1                 /* Core where the task should run */
    );
}