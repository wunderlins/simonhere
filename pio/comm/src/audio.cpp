#include "audio.h"
#include "samples_data.h"
#include "main.h"

audio_files_t audio_files;
static TaskHandle_t task_play_audio = NULL;
static int playlist[11] = {0};

static void play_task();

void setup_audio() {
    audioLogger = &Serial;
}

static int playlist_length() {
    for(int i=0; i<10; i++) {
        if(playlist[i] == 0) {
            return i;
        }
    }
    return -1;
}

static int playlist_push(int index) {
    int l = playlist_length();
    if (l == -1) return l;
    playlist[l] = index;

#if DEBUGGING > 0
    output.printf("Adding file %d at position %d\n", index, l);
#endif
    return l;
}

static int playlist_pop() {
    if (playlist[0] == 0)
        return -1;
    
    // move every element down one position
    int ret = playlist[0];
    for (int i=0; i<10; i++) {
        playlist[i] = playlist[i+1];
    }

    return ret;
};

static void task_play(void* parameter) {

    // run until all files in the queue have been played
    while(playlist[0] != 0) {
        audio_files_t index = (audio_files_t) playlist_pop();

        if (index == -1) // playlist is empty?
            return;
        
#if DEBUGGING > 0
        output->printf("Playing: %d\n", index);
#endif
        AudioFileSourcePROGMEM* file = new AudioFileSourcePROGMEM(AUDIO[index], AUDIO_len[index]);
        AudioGeneratorMP3* wav = new AudioGeneratorMP3();

        AudioOutputI2S* out = new AudioOutputI2S();
        out->SetGain(1);
        out->SetPinout(I2S_BLCK, I2S_LRC, I2S_DOUT);

        wav->begin(file, out);

        // check if audio is still running
        while (wav->isRunning()) {

            // we are done, remove task
            if (!wav->loop())  {
                wav->stop();
#if DEBUGGING > 0
                output->printf("Done with %d\n", index);
#endif
            }

            vTaskDelay(20 / portTICK_PERIOD_MS);
        }
    }

    task_play_audio = NULL;
    vTaskDelete(NULL); // delete this process

}

static void play_task() {
    xTaskCreatePinnedToCore(
        task_play,        /* Task function. */
        "task_play",      /* String with name of task. */
        10000,            /* Stack size in bytes. */
        NULL,             /* Parameter passed as input of the task */
        1,                /* Priority of the task. */
        &task_play_audio, /* Task handle. */
        1                 /* Core where the task should run */
    );
}

void play(audio_files_t index) {

    // add new file to playlist
    int ret = playlist_push(index);
    if (ret == -1) {
        output->printf("Playlist full, could not queue file %d\n", index);
        return;
    }

    if (task_play_audio == NULL)
        play_task();
}

void cmd_play(cmd_t* cmd) {
    // check length
    if (cmd->length < 1) {
        output->printf("Invalid argument length\n");
        return;
    }

    for (int i=1; i<cmd->length; i++) {
        audio_files_t index = (audio_files_t) atoi(cmd->arg[i]);
        if (index >= audio_files_len) {
            output->printf("Invalid file name %d\n", index);
            return;
        }

        play(index);
    }
}