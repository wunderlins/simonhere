#include "wifi.h"

void setup_wifi() {
    // watch gpio for state change
    attachInterrupt(STATUS_PIN, state_wifi_change, CHANGE);
}

void state_wifi_change() {
    delay(20); // let the pin settle
    bool s = digitalRead(STATUS_PIN);
    //Serial.printf("wifi status: %d, current status: %d\n", s, state.wifi);
    
    if (s != state.wifi) {

        state.wifi = s;
        if (s) {
            state.tst_wifi = time(NULL);
        } else {
            state.tst_wifi = 0;
        }

#if DEBUGGING_AUDIO > 0    
        // give feedback
        if (state.wifi == 1) {
            play(WIFI);
            play(CONNECTED);
        } else {
            play(WIFI);
            play(TIMEOUT);
        }
#endif
    }

    recalculate_state();
}