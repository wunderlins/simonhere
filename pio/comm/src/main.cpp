/**
 * Command & Control 
 * 
 * This is the CNC decide which is connected to all surrounding 
 * devices via serial connections.
 * 
 * It tracks state of 
 * - AirSpy (WIFI)
 * - AirSpy (BT)
 * - FaceCam
 * - PC Lock Screen
 */
#include "state.h"
#include "wifi.h"
#include "console.h"
#include "audio.h"
#include "datetime.h"
#include "main.h"

#if BT_ENABLED > 0
#include "BluetoothSerial.h"
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
#endif
Stream* output = &Serial;

void setup() {
  setup_state();
  setup_wifi();
  setup_console();

#if BT_ENABLED > 0
  SerialBT.begin("AirSPY"); //Bluetooth device name
  // Serial.println("The device started, now you can pair it with bluetooth!");
  /*
  delay(1000); // wait for modem to connect
  SerialBT.println("sync");
  */

  // start time sync task
  sync_task();
#endif

}

char c = 0;
char line[80];
int  i = 0;

void serve_serial(Stream *device) {
  output = device;

  if (output->available()) {

    while(output->available()) {
      if (i > 80) {
        output->println("\r\nERR: input buffer overflow, command ignored");
        console_prompt(output);
        while (output->read() != -1); // clear buffer
        i=0;
        line[0] = 0;
        continue;
      }
      c = output->read();

      if (c == 10 || c == 13) {
        line[i] = 0; // null terminate string

        // some terminals send CR+LF which results in empty lines
        if (i > 0 && line[0] != 10 && line[0] != 13) {
          console_prompt(output);
          output->println(line);
          
          // tokenize command
          cmd_t* cmd = cmd_new(line, i);

#if DEBUGGING > 0
          Serial.print("\r");
          for (int ii=0; ii<cmd->length; ii++)
            Serial.printf("» %d %s\n", ii, cmd->arg[ii]);
#endif
          
          // do something with the command
          command_t cmd_id = cmd_valid(cmd);
#if DEBUGGING > 0
          Serial.printf("»»»»» Command to run: %d\n", cmd_id);
#endif
          if (cmd_id < CMD_HELP) {
            output->printf("Invalid command: %s, %d\r\n", line, cmd_id);
          }

          // FIXME: we could use a lookup table for the functions
          switch (cmd_id) {
            case CMD_INVALID:
              cmd_invalid(cmd);
              break;
            
            case CMD_UNKOWN:
              cmd_unknown(cmd);
              break;
            
            case CMD_HELP:
              cmd_help(cmd);
              break;
            
            case CMD_SAY:
              cmd_play(cmd);
              break;
            
            case CMD_STATUS:
              cmd_state(cmd);
              break;
            
            case CMD_TIME:
              cmd_time(cmd);
              break;
            
            case CMD_SCREENLOCK:
              cmd_screenlock(cmd);
              break;
            
            default:
              break;
          }

          cmd_free(cmd);

        }
        
        // reset line buffer
        console_prompt(output);
        i=0;
        continue;
      }
      
      if (c == 8) {
        line[--i] = 0;
        output->print('\r');
        output->print("                                                                                ");
        console_prompt(output);
        output->print(line);
      } else {
        output->print(c);
        line[i++] = c;
      }
    }
  }

  delay(50);
}

void loop() {
  // serve usb serial
  serve_serial(&Serial);

#if BT_ENABLED > 0
  // serve bluetooth serial
  serve_serial(&SerialBT);
#endif

  //Serial.printf("»» Status pin: %d\n", digitalRead(STATUS_PIN));

}