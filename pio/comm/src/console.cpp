#include "console.h"
#include "main.h"

// list of avialble commands
const char* commands[] = {
    "help",   // 0
    "say",    // 1
    "state",  // 2
    "time",   // 3
    "screenlock", // 4
    NULL,
};

// print banner
const char banner[350] PROGMEM = "\r\n"
    "  _|_|    _|              _|_|_|  _|_|_|    _|      _|\r\n"
    "_|    _|      _|  _|_|  _|        _|    _|    _|  _|\r\n"
    "_|_|_|_|  _|  _|_|        _|_|    _|_|_|        _|\r\n"
    "_|    _|  _|  _|              _|  _|            _|\r\n"
    "_|    _|  _|  _|        _|_|_|    _|            _|\r\n"
    "\r\n";


// the setup function runs once when you press reset or power the board
void setup_console() {
	
	Serial.begin(115200);
	// wait for serial
	delay(50);
	
	output->print(banner);
	output->println("");
    console_prompt(&Serial);
}

void console_prompt(Stream *device) {
  device->print("\r> ");
}

void cmd_free(cmd_t* cmd) {
    for(int i=0; i<cmd->length; i++)
        free(cmd->arg[i]);
    free(cmd);
}

command_t cmd_valid(cmd_t* cmd) {
    if (cmd->length < 1)
        return CMD_INVALID;
    
    int i = 0;
    while (commands[i] != NULL) {
        //output->printf("Comparing: %s %s\n", commands[i], cmd->arg[0]);
        if (strcmp(commands[i], cmd->arg[0]) == 0)
            return (command_t) i;
        i++;
    }

    return CMD_UNKOWN;
}

cmd_t* cmd_new(char* line, int length) {
    // local variables
    char c = 0;
    char buffer[80] = {0};
    int b = 0;

    //output->printf("\nlength: %d, strlen(line): %d\n", length, strlen(line));

    // return value
    cmd_t* cmd = (cmd_t*) malloc(sizeof(cmd_t));
    cmd->length = 0;

    //output->print("length: ");
    //output->println(length);

    for(int i=0; i<length; i++) {
        c = line[i];
        /*
        Serial.print(i);
        Serial.print(" ");
        Serial.print(b);
        Serial.print(" ");
        Serial.println(c);
        */

        if (c == ' ' || i == length-1) {
            // skip double spaces
            if (b == 0 && c == ' ') continue;

            // add last char to buffer
            if (i == length-1) {
                //Serial.print("last char");
                buffer[b++] = c;
                buffer[b] = 0;
            }

            // prevent buffer over runs with too many arguments
            if (cmd->length == CMD_MAX_ARGS) {
                cmd_free(cmd);
                return NULL;
            }

            // new token;
            buffer[b] = 0; // terminate string
#if DEBUGGING > 0
            output->print("Buffer: ");
            output->println(buffer);
#endif
            cmd->arg[cmd->length] = (char*) malloc(b+1);
            memcpy(cmd->arg[cmd->length], buffer, b+1);
            cmd->length++;

            // reset buffer
            b=0;
            buffer[0] = 0;
        } else {
            buffer[b++] = c;
            buffer[b] = 0;
        }
    }

    return cmd;
}

void help() {
    int i = 0;
    output->println("Commands:");
    while (commands[i] != NULL) {
        output->print(" » ");
        output->println(commands[i++]);
    }
}

void cmd_invalid(cmd_t* cmd) {
    output->printf("Invalid command\r\n");
}

void cmd_unknown(cmd_t* cmd) {
    output->printf("Unknown command: '%s'\r\n", cmd->arg[0]);
}

void cmd_help(cmd_t* cmd) {
	output->print(banner);
	output->println("");
    help();
}
