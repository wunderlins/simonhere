#include "config.h"

/*
// how long to stay on a channel in milli seconds
int WIFI_CHANNEL_SWITCH_INTERVAL = 500;

// maximum channel to scan for
uint8_t WIFI_CHANNEL_MAX = 13;

// minimum RSSI in dBm level for a packet to be teated as near enough
int WIFI_MIN_RSSI = -80;

// WIFI MAC address we are looking for
//uint8_t scan_for[6] = {0xA4, 0x50, 0x46, 0xC5, 0xEC, 0xC9};
uint8_t scan_for[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

// how long do we assume the device is still in range since after 
// seeing it the last time (in seconds) ?
uint8_t max_last_seen = 180;
*/

// air modem baud rate
const int MODEM_BAUD = 57600;

