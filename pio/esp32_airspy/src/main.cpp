#include "Arduino.h"
#include <EEPROM.h>

#include "globals.h"
#include "config.h"
#include "console.h"
#include "wifi.h"
#include "modem.h"

TaskHandle_t WIFI_Scan, Modem, Console;

airspy_state_t airspy_state;

// default config
airspy_config_t airspy_config; /* = {
	.mac = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
	.scan_delay = 500,
	.min_rssi  = 80,
	.wifi_chan_max = 13,
	.wifi_chan_switch_int = 500,
	.max_last_seen = 500
}; */

// the setup function runs once when you press reset or power the board
void setup() {
	
	// initialize EEPROM with predefined size
	EEPROM.begin(EEPROM_SIZE);
	delay(150);
	
	// read configuration from EEEPROM to airspy_config
	getStruct(); 
	
	// load console and config
	setup_console();
	
	setup_wifi();
	setup_modem();
	
	delay(50);
	modemSet(false);
	
	
	xTaskCreatePinnedToCore(
	  TaskWIFIScan
	  ,  "WIFIScan"
#ifdef DEBUG_WIFI
	  ,  2048  // Stack size
#else
	  ,  1024  // Stack size
#endif
	  ,  NULL
	  ,  2  // Priority
	  ,  &WIFI_Scan 
	  ,  1);
	
	xTaskCreatePinnedToCore(
	  TaskConsole
	  ,  "Console"
	  ,  2048  // Stack size
	  ,  NULL
	  ,  1  // Priority
	  ,  &Console // TaskHandle 
	  ,  0); // core
	
}

extern wifi_scanner_state_t wifi_state;

// the loop function runs over and over again forever
void loop() {
	// check if last seen timeout for wifi is reached
	checkWifiTimeout();
	
	// report current state
	if (airspy_state.mode == SCANNING) {
		unsigned long timeout = ((millis() - wifi_state.last_seen)/1000);
		printf("WIFI state: %d, since sec: %lu/%d\n", 
		       wifi_state.connected, timeout, airspy_config.max_last_seen);
	
		//printf("last_seen: %lu, millis: %lu\n", wifi_state.last_seen, millis());
	}
	vTaskDelay(airspy_config.scan_delay / portTICK_PERIOD_MS);
	
}

