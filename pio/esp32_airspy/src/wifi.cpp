#include "wifi.h"
#include "modem.h"
#include "globals.h"

static uint8_t channel = 1;
static wifi_country_t wifi_country = {
	.cc="CH", 
	.schan = 1, 
	.nchan = airspy_config.wifi_chan_max
}; //Most recent esp32 library struct
wifi_scanner_state_t wifi_state;

esp_err_t event_handler(void *ctx, system_event_t *event)
{
  return ESP_OK;
}

void wifi_sniffer_init(void)
{
  nvs_flash_init();
  tcpip_adapter_init();
  ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
  ESP_ERROR_CHECK( esp_wifi_set_country(&wifi_country) ); /* set country for channel range [1, 13] */
  ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
  ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_NULL) );
  ESP_ERROR_CHECK( esp_wifi_start() );
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_promiscuous_rx_cb(&wifi_sniffer_packet_handler);
}

void wifi_sniffer_set_channel(uint8_t channel)
{
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
}

const char * wifi_sniffer_packet_type2str(wifi_promiscuous_pkt_type_t type)
{
  switch(type) {
  case WIFI_PKT_MGMT: return "MGMT";
  case WIFI_PKT_DATA: return "DATA";
  default:  
  case WIFI_PKT_MISC: return "MISC";
  }
}

bool compare_addr(const uint8_t *addr, const uint8_t *compto) {
  if (
    addr[0] == compto[0] && 
    addr[1] == compto[1] && 
    addr[2] == compto[2] && 
    addr[3] == compto[3] && 
    addr[4] == compto[4] && 
    addr[5] == compto[5]
  ) {
    return true;
  }
  return false;
}

int last_seen_s;
void wifi_sniffer_packet_handler(void* buff, wifi_promiscuous_pkt_type_t type)
{
  //if (type != WIFI_PKT_MGMT) return;

  const wifi_promiscuous_pkt_t *ppkt = (wifi_promiscuous_pkt_t *)buff;
  const wifi_ieee80211_packet_t *ipkt = (wifi_ieee80211_packet_t *)ppkt->payload;
  const wifi_ieee80211_mac_hdr_t *hdr = &ipkt->hdr;

  // have we see na packte that we are interested in ?
  if ((compare_addr(airspy_config.mac, hdr->addr1) || 
		   compare_addr(airspy_config.mac, hdr->addr2))
       && ppkt->rx_ctrl.rssi > (airspy_config.min_rssi * -1)) {
		// update state
		unsigned long l = wifi_state.last_seen;
		wifi_state.last_seen = millis();
		last_seen_s = (wifi_state.last_seen - l) / 1e3;
		wifi_state.connected = CONNECTED;
		modemSet(true);
	
		// display scanner info 
		if (airspy_state.mode == SCANNING && last_seen_s > 0)
	    printf("WIFI: %ds, CHAN=%02d, RSSI=%02d,"
	      " %02x:%02x:%02x:%02x:%02x:%02x -> "
	      " %02x:%02x:%02x:%02x:%02x:%02x\n",
	      //wifi_sniffer_packet_type2str(type),
	      last_seen_s, 
	      ppkt->rx_ctrl.channel,
	      ppkt->rx_ctrl.rssi,
	      /* ADDR1 */
	      hdr->addr2[0],hdr->addr2[1],hdr->addr2[2],
	      hdr->addr2[3],hdr->addr2[4],hdr->addr2[5],
	      /* ADDR2 */
	      hdr->addr1[0],hdr->addr1[1],hdr->addr1[2],
	      hdr->addr1[3],hdr->addr1[4],hdr->addr1[5]
	    );
  }
}

void checkWifiTimeout() {
	// do not check anything if we are not in connected state
	if (wifi_state.connected != CONNECTED)
		return;
	
	unsigned long t = millis();
#ifdef DEBUG_WIFI
	//printf("WIFI DEBUG: %d %d\n", (int)((t - wifi_state.last_seen)/1e3),
	//                                max_last_seen);
#endif
	if (t - wifi_state.last_seen > (airspy_config.max_last_seen * 1e3)) {
		// change state
		wifi_state.connected = TIMEOUT;
		modemSet(false);
		
		if (airspy_state.mode == SCANNING)
			printf("WIFI timeout %ds reached\n", airspy_config.max_last_seen);
	}
	
}


// the setup function runs once when you press reset or power the board
void setup_wifi() {
	printf("Sniffing WIFI for: %x:%x:%x:%x:%x:%x\n", 
	       airspy_config.mac[0], 
	       airspy_config.mac[1], 
	       airspy_config.mac[2], 
	       airspy_config.mac[3], 
	       airspy_config.mac[4], 
	       airspy_config.mac[5]);
	printf("Minimal sensitivity (dBm): %d\n", airspy_config.min_rssi);
	wifi_sniffer_init();
}

void TaskWIFIScan(void *pvParameters)
{
	(void) pvParameters;

	for (;;)
	{
		
		// the loop function runs over and over again forever
		vTaskDelay(airspy_config.wifi_chan_switch_int / portTICK_PERIOD_MS);
		wifi_sniffer_set_channel(channel);
		channel = (channel % airspy_config.wifi_chan_max) + 1;
		
#ifdef DEBUG_WIFI
		printf("DEBUG WIFI: channel %d\n", channel);
		//delay(50);
#endif
		
	}
}
