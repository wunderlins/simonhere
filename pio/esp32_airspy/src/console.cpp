#include "Arduino.h"
// include library to read and write from flash memory
#include <EEPROM.h>
#include "config.h"
#include "globals.h"
#include "console.h"

// the setup function runs once when you press reset or power the board
void setup_console() {
	
	Serial.begin(115200);
	// wait for serial
	delay(250);
	
	// print banner
	const char banner[350] = "\n"
		"  _|_|    _|              _|_|_|  _|_|_|    _|      _|\n"
		"_|    _|      _|  _|_|  _|        _|    _|    _|  _|\n"
		"_|_|_|_|  _|  _|_|        _|_|    _|_|_|        _|\n"
		"_|    _|  _|  _|              _|  _|            _|\n"
		"_|    _|  _|  _|        _|_|_|    _|            _|\n"
		"\n"
		"Enter \"$$$\" for command mode, leave command mdoe with \"+++\"."
		"\n\nConfiguration:\n";
	Serial.print(banner);
	
	print_all();
	Serial.print("\n");
	
}

void help() {
	Serial.print("\n"
		"Console Commands:\n"
		"  set <key> <value>\n"
		"  - mac <XX:XX:XX:XX:XX:XX>\n"
		"  - rssi <int>: minmal rssi in dBm (default -80)\n"
		"  - main_loop_delay <int>: how long to wait until the next scan in ms\n"
		"  - max_chan <int>: maximum wifi channels to scan (default: 13)\n"
		"  - chan_interval <int>: how long to stay on a channel in ms\n"
		"  - last_seen <int>: how long until we decide the device is gone in ms\n\n"
		" get [key]\n"
		"  - key: optionl, same keys as with set\n"
		"    if empty, all configuration keys are shown\n"
		"\n"
		"Leave command mode with \"+++\"."
		"\n"
	);
}

/**
 * Display EEEPROM values
 * 
 * @arg tokenized arguments
 * @arg number of arguments
 */
void cmd_get(char args[8][20], int nargs) {
	
	if (strcmp(args[1], "mac") == 0) {
		print_mac();
		return;
	}
	
	if (strcmp(args[1], "rssi") == 0) {
		print_min_rssi();
		return;
	}
	
	if (strcmp(args[1], "main_loop_delay") == 0) {
		print_scan_delay();
		return;
	}
	
	if (strcmp(args[1], "max_chan") == 0) {
		print_wifi_channel_max();
		return;
	}
		
	if (strcmp(args[1], "chan_interval") == 0) {
		print_wifi_channel_switch_interval();
		return;
	}
	
	if (strcmp(args[1], "last_seen") == 0) {
		print_max_last_seen();
		return;
	}
	
	if (strcmp(args[1], "json") == 0) {
#ifdef DEBUG_CONSOLE
		Serial.println("Generate json output");
#endif
		print_json_data();
		return;
	}
	
	// show all
#ifdef DEBUG_CONSOLE
	Serial.println("DEBUG CONSOLE: no specific key");
#endif
	print_all();
}

/**
 * Set EEEPROM values
 * 
 * @arg tokenized arguments
 * @arg number of arguments
 */
void cmd_set(char args[8][20], int nargs) {
	
	if (strcmp(args[1], "mac") == 0) {
		// parse arg[1], it should contain a colon separated max addr
		uint8_t addr[6];
		int i, l = strlen(args[2]);
		char buff[5] = {0};
		int buff_p = 0;
		int addr_p = 0;
		
		// FIXME: add bounds check
		for (i=0; i<l; i++) {
			// colong found, add int to addr at pos
			char c = args[2][i];
			if (c == ':') {
				addr[addr_p++] = strtol(buff, NULL, 16);
				buff[0] = 0;
				buff_p = 0;
				continue;
			}
			
			buff[buff_p++] = c;
			buff[buff_p]   = 0; // terminate
		}
		
		// add last byte
		addr[addr_p] = strtol(buff, NULL, 16);
		
		if (addr_p != 5) {
			Serial.print("Error: wrong length (");
			Serial.print(addr_p);
			Serial.println(") mac address");
			return;
		}
	
#ifdef DEBUG_CONSOLE
		// show parsed address
		print_mac();
#endif
		
		// store in EEPROM
		airspy_config.mac[0] = addr[0];
		airspy_config.mac[1] = addr[1];
		airspy_config.mac[2] = addr[2];
		airspy_config.mac[3] = addr[3];
		airspy_config.mac[4] = addr[4];
		airspy_config.mac[5] = addr[5];
		setStruct();
		
		return;
	}
	
	if (strcmp(args[1], "rssi") == 0) {
		int rssi = atoi(args[2]);
		if (rssi < 0) rssi = rssi * -1;
		airspy_config.min_rssi = rssi;
		setStruct();
		return;
	}
	
	if (strcmp(args[1], "main_loop_delay") == 0) {
		airspy_config.scan_delay = atoi(args[2]);
		setStruct();
		return;
	}
	
	if (strcmp(args[1], "max_chan") == 0) {
		airspy_config.wifi_chan_max = atoi(args[2]);
		setStruct();
		return;
	}
		
	if (strcmp(args[1], "chan_interval") == 0) {
		airspy_config.wifi_chan_switch_int = atoi(args[2]);
		setStruct();
		return;
	}
	
	if (strcmp(args[1], "last_seen") == 0) {
		airspy_config.max_last_seen = atoi(args[2]);
		setStruct();
		return;
	}
	
	printf("Unknown value '%s', use \"help\" for more information\n", args[1]);
}

void run_command(char args[8][20], int nargs) {
	// output all arguments
	
#ifdef DEBUG_CONSOLE
	int i = 0;
	Serial.println("Debug output, words: ");
	Serial.print(nargs);
	Serial.println("");
	for(; i<nargs; i++) {
		Serial.print(i);
		Serial.print(": '");
		Serial.print(args[i]);
		Serial.println("'");
	}
#endif
	
	if (strcmp(args[0], "$$$") == 0) {
		airspy_state.mode = CONSOLE;
		help();
#ifdef DEBUG_CONSOLE
		Serial.println("DEBUG CONSOLE: mode console");
#endif
		return;
	}
	
	if (strcmp(args[0], "+++") == 0) {
		airspy_state.mode = SCANNING;
#ifdef DEBUG_CONSOLE
		Serial.println("DEBUG CONSOLE: mode scanning");
#endif
		return;
	}
	
	if (args[0][0] == 'h') {
		help();
		return;
	}

	if (strcmp(args[0], "get") == 0) {
		cmd_get(args, nargs);
		return;
	}

	if (strcmp(args[0], "set") == 0) {
		cmd_set(args, nargs);
		return;
	}
	
	printf("Unknown command '%s', use \"help\" for more information\n", args[0]);
}

void show_prompt() {
	Serial.print("> ");
}

void TaskConsole(void *pvParameters) {
	
	// task specific variables
	(void) pvParameters;
	
	// 8 args max, 20 char each, chech input
	char c;
	int cursor_args = 0;
	int cursor_cmd  = 0;
	char args[8][20] = {0}; 
	
	// show prompt
	if (airspy_state.mode == CONSOLE)
		show_prompt();
	
	// task loop
	for (;;) {
		
		// Input tokenizer
		//
		// read string upon first '\n', tokenize every word into the cmd array
		while (Serial.available()) {
			
			// read byte
			c = Serial.read();
			
			// skip multiple spaces
			if (cursor_cmd == 0 && 
			   (c == ' ' || c == '\n' || c == '\r'))
				continue;
			
			// echo Character
			if (airspy_state.mode == CONSOLE)
				Serial.write(c);
			
			// if the character is a space ' ', jump to next arg
			if (c == ' ') {
				// FIXME: add bounds check
				args[cursor_args][cursor_cmd] = 0; // terminate command
				cursor_args++;                     // jump to next arg
				cursor_cmd = 0;                    // rewind arg position
				continue;                          // we are done
			}
			
			// '\n' means end of command string
			// - run the command
			// - reset the parser and then 
			if (c == '\n' || c == '\r') {
				// terminate command
				if (c == '\n')
					//args[cursor_args][cursor_cmd-1] = 0;
					args[cursor_args][cursor_cmd] = 0;
				else if (c == '\r')
					args[cursor_args][cursor_cmd] = 0;
				
				// increment args count
				cursor_args++;
				
				// run command
				run_command(args, cursor_args);
				
				// reset
				cursor_args = 0;
				cursor_cmd  = 0;
				
				// reset all args
				args[0][0]  = 0;
				args[1][0]  = 0;
				args[2][0]  = 0;
				args[3][0]  = 0;
				args[4][0]  = 0;
				args[5][0]  = 0;
				
				// show prompt
				if (airspy_state.mode == CONSOLE)
					show_prompt();
				
				continue;
			}
			
			// any other character than '\' and ' ' will be appended to the current 
			// argument
			args[cursor_args][cursor_cmd++] = c;
			// FIXME: add bounds check
			
		}
		
		
		// give other tasks the opportunity to do something
		vTaskDelay(200 / portTICK_PERIOD_MS);
	}
}
