#include "Arduino.h"

#include "globals.h"
#include "modem.h"

#define STATUS_PIN 18

void setup_modem() {
  Serial.print("Air Modem Setup: ");
  Serial.print(MODEM_BAUD);
  Serial.println(" Baud");

  /**
   * Serial2 on WEMOS D1 ESP32
   * - TX: IO17
   * - RX: IO16
   */
  Serial2.begin(MODEM_BAUD);
	
	// status LED, on when simone is here, off otherwise
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(STATUS_PIN, OUTPUT);
}

void modemSet(bool state) {
  if (state) {
#ifdef DEBUG_MODEM
		if (airspy_state.mode == SCANNING)
	    Serial.println("Sending ON Command");
#endif
    Serial2.println(1);
		digitalWrite(LED_BUILTIN, HIGH);
		digitalWrite(STATUS_PIN, HIGH);
  } else {
#ifdef DEBUG_MODEM
		if (airspy_state.mode == SCANNING) {
	    Serial.println("Sending OFF Command");
#endif
    Serial2.println(0);
		digitalWrite(LED_BUILTIN, LOW);
		digitalWrite(STATUS_PIN, LOW);
  }
}
