#include "Arduino.h"
#include <EEPROM.h>
#include "config.h"
#include "globals.h"

void setStruct() {
	EEPROM.put(EEPROM_STRUCT_ADDR, airspy_config);
	EEPROM.commit();
}
void getStruct() {
	EEPROM.get(EEPROM_STRUCT_ADDR, airspy_config);
}

/*
uint8_t * getMac() {
	static uint8_t buff[6] = {0,0,0,0,0,0};
	int i = 0;
	for (i=0; i<6; i++) {
		buff[i] = EEPROM.readByte(MAC+i);
	}
	return buff;
}
*/


// Display EEPROM values
void print_mac_value() {
	Serial.print(airspy_config.mac[0], HEX); Serial.print(':');
	Serial.print(airspy_config.mac[1], HEX); Serial.print(':');
	Serial.print(airspy_config.mac[2], HEX); Serial.print(':');
	Serial.print(airspy_config.mac[3], HEX); Serial.print(':');
	Serial.print(airspy_config.mac[4], HEX); Serial.print(':');
	Serial.print(airspy_config.mac[5], HEX);
}

void print_mac() {
	// MAC Address
	Serial.print("mac:             ");
	print_mac_value();
  Serial.print('\n');
	
}

void print_min_rssi() {
	Serial.print("rssi:            ");
	Serial.print("-"); 
	Serial.print(airspy_config.min_rssi);
	Serial.println(" dBm"); 
}

void print_scan_delay() {
	Serial.print("main_loop_delay: ");
	Serial.print(airspy_config.scan_delay);
	Serial.println(" ms"); 
}

void print_wifi_channel_max() {
	Serial.print("max_chan:        ");
	Serial.print(airspy_config.wifi_chan_max);
	Serial.println(" (USA: 14, Europe: 13)"); 
}

void print_wifi_channel_switch_interval() {
	Serial.print("chan_interval:   ");
	Serial.print(airspy_config.wifi_chan_switch_int);
	Serial.println(" ms"); 
}

void print_max_last_seen() {
	Serial.print("last_seen:       ");
	Serial.print(airspy_config.max_last_seen);
	Serial.println(" Seconds"); 
}

void print_json_data() {
	Serial.print("{");
	// mac
	Serial.print("\"mac\":\"");
	print_mac_value();
	Serial.print("\", ");

	// rssi
	Serial.print("\"min_rssi\":");
	Serial.print(airspy_config.min_rssi);
	Serial.print(", ");

	// main_loop_delay
	Serial.print("\"scan_delay\":");
	Serial.print(airspy_config.scan_delay);
	Serial.print(", ");

	// max_chan
	Serial.print("\"wifi_chan_max\":");
	Serial.print(airspy_config.wifi_chan_max);
	Serial.print(", ");

	// chan_interval
	Serial.print("\"wifi_chan_switch_int\":");
	Serial.print(airspy_config.wifi_chan_switch_int);
	Serial.print(", ");

	// last_seen
	Serial.print("\"max_last_seen\":");
	Serial.print(airspy_config.max_last_seen);
	Serial.println("}");

	// printf("mac: %.2X\n", airspy_config.mac[0]);
}

void print_all() {
	print_mac();
	print_min_rssi();
	print_scan_delay();
	print_wifi_channel_max();
	print_wifi_channel_switch_interval();
	print_max_last_seen();
}
