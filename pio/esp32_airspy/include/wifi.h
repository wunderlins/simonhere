#ifndef WIFI_H
#define WIFI_H

#include "Arduino.h"

#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_wifi_types.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"

#include "config.h"

// enable serial debugging for WIFI
//#define DEBUG_WIFI // comment out to disable wifi debugging

typedef enum {
	CONNECTED = 0,
	UNKNOWN,
	TIMEOUT
} wifi_state_t;

typedef struct {
	unsigned long last_seen = 0LU;
	wifi_state_t connected = UNKNOWN;
} wifi_scanner_state_t;

typedef struct {
  unsigned frame_ctrl:16;
  unsigned duration_id:16;
  uint8_t addr1[6]; /* receiver address */
  uint8_t addr2[6]; /* sender address */
  uint8_t addr3[6]; /* filtering address */
  unsigned sequence_ctrl:16;
  uint8_t addr4[6]; /* optional */
} wifi_ieee80211_mac_hdr_t;

typedef struct {
  wifi_ieee80211_mac_hdr_t hdr;
  uint8_t payload[0]; /* network data ended with 4 bytes csum (CRC32) */
} wifi_ieee80211_packet_t;


esp_err_t event_handler(void *ctx, system_event_t *event);
void wifi_sniffer_init(void);
void wifi_sniffer_set_channel(uint8_t channel);
const char * wifi_sniffer_packet_type2str(wifi_promiscuous_pkt_type_t type);
void wifi_sniffer_packet_handler(void* buff, wifi_promiscuous_pkt_type_t type);

// the setup function runs once when you press reset or power the board
void setup_wifi();

// the loop function runs over and over again forever
void TaskWIFIScan(void *pvParameters);

// check if we have reached wifi last seen timeout
void checkWifiTimeout();

#endif // WIFI_H
