#ifndef CONFIG_H
#define CONFIG_H

#include "stdint.h"
#include "globals.h"

/*
// WIFI MAC address we are looking for
extern uint8_t scan_for[6];

// how long do we assume the device is still in range since after 
// seeing it the last time (in seconds) ?
extern uint8_t max_last_seen;

extern int WIFI_CHANNEL_SWITCH_INTERVAL;
extern uint8_t WIFI_CHANNEL_MAX;

extern int WIFI_MIN_RSSI;
*/

#define EEPROM_STRUCT_ADDR 0

enum airspy_state_mode_t {
	SCANNING = 0,
	CONSOLE  = 1
};

struct airspy_state_t {
	int mode = SCANNING; 
};

// default configuration, if EEPROM is empty
struct airspy_config_t {
	uint8_t  mac[6] = {255, 255, 255, 255, 255, 255};
	uint16_t scan_delay = 500;
	uint8_t min_rssi = 80;
	uint8_t wifi_chan_max = 13;
	uint16_t wifi_chan_switch_int = 250;
	uint16_t max_last_seen = 180;
};

const int EEPROM_SIZE = sizeof(airspy_config_t);

extern const int MODEM_BAUD;

// globals defined in main.cpp
extern airspy_state_t airspy_state;
extern airspy_config_t airspy_config;


#endif