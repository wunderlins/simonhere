#ifndef MODEM_H
#define MODEM_H

#include "Arduino.h"
#include "config.h"

// enable serial debugging for MODEM
//#define DEBUG_MODEM // comment out to disable wifi debugging

void setup_modem();
void modemSet(bool state);

#endif // MODEM_H
