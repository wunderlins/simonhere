#ifndef GLOBALS_H
#define GLOBALS_H

// EEPROM Helper
void getStruct();
void setStruct();


// Display EEPROM Values
void print_mac();
void print_min_rssi();
void print_scan_delay();
void print_wifi_channel_max();
void print_wifi_channel_switch_interval();
void print_max_last_seen();
void print_json_data();
void print_all();

#endif // GLOBALS_H
