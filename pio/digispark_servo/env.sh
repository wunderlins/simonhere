#!/usr/bin/env bash

function init() {
	pio run
}

function monitor() {
	pio device monitor
}

function build() {
	pio run -e digispark-tiny
}

function upload() {
	pio run -e digispark-tiny -t upload
}
