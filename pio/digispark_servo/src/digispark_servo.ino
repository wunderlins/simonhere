/**
 * Serial receiver 
 * 
 * Receives data on software serial, switches servo and LED accordingly
 *  
 * The circuit: 
 *   * RX is digital pin 2 (connect to TX of other device)
 *   * TX is digital pin 3 (connect to RX of other device)
 *   * SERVO is conencted to pin 0
 *   * LED connected to pin 1
 * 
 * This software is written for ATTiny85 with micronucleus bootloader (Digispark)
 * Compiled with Arduino 1.8.9
 * 
 * 2020, Simon Wunderlin
 */

#include <SoftSerial.h>     /* Allows Pin Change Interrupt Vector Sharing */
#include <TinyPinChange.h>  /* Ne pas oublier d'inclure la librairie <TinyPinChange> qui est utilisee par la librairie <RcSeq> */
#include <SimpleServo.h>

// pin definitions
#define PIN_SERVO_PWM 0
#define LED_BUILTIN 1
#define PIN_RX 2
#define PIN_TX 3

// servo Positions
#define SERVO_MIN 0
#define SERVO_MAX 120
#define DELAY 500

// globals
SimpleServo servo;
SoftSerial mySerial(PIN_RX, PIN_TX); // RX, TX
bool state = false;
bool state_next = false;
char c;
unsigned long time;

void setup()  
{
  // set the data rate for the SoftwareSerial port
  mySerial.begin(57600);
  
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  // init servo
  servo.attach(PIN_SERVO_PWM);
  servo.write(SERVO_MIN); // home
}

void loop() // run over and over
{ 
  // check if modem has to tell us something
  if (mySerial.available()) {
    c = mySerial.read();
    mySerial.write(c);

    // memmorize last sent state
    if (c == '1')
      state_next = true;
    if (c == '0')
      state_next = false;
  }

  // delay servo update if there has been a state change within DELAY
  if (millis() < time) {
    return;
  }
  
  // check if state has changed in the meantime and switch servo
  if (state != state_next) {
      if (state_next) {
        digitalWrite(LED_BUILTIN, HIGH);
        servo.write(SERVO_MAX);
      } else {
        digitalWrite(LED_BUILTIN, LOW);
        servo.write(SERVO_MIN);        
      }
      time = millis() + DELAY; // prevent flip flopping and brown out
      state = state_next;
  }
}
