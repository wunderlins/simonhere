#include "esp_camera.h"
#include "img_converters.h"
#include "camera_index.h"
#include "Arduino.h"

#include "fb_gfx.h"
#include "fd_forward.h"
#include "fr_forward.h"
#include "camera_func.h"

#define ENROLL_CONFIRM_TIMES 5
#define FACE_ID_SAVE_NUMBER 7

static mtmn_config_t mtmn_config = {0};
static face_id_list id_list = {0};
static int is_enrolling = 0;

void mtmn_config_init() {
    mtmn_config.type = FAST;
    mtmn_config.min_face = 80;
    mtmn_config.pyramid = 0.707;
    mtmn_config.pyramid_times = 4;
    mtmn_config.p_threshold.score = 0.6;
    mtmn_config.p_threshold.nms = 0.7;
    mtmn_config.p_threshold.candidate_number = 20;
    mtmn_config.r_threshold.score = 0.7;
    mtmn_config.r_threshold.nms = 0.7;
    mtmn_config.r_threshold.candidate_number = 10;
    mtmn_config.o_threshold.score = 0.7;
    mtmn_config.o_threshold.nms = 0.7;
    mtmn_config.o_threshold.candidate_number = 1;
}

void camera_init() {
    mtmn_config_init();
    face_id_init(&id_list, FACE_ID_SAVE_NUMBER, ENROLL_CONFIRM_TIMES);
}

static int run_face_recognition(dl_matrix3du_t *image_matrix, box_array_t *net_boxes){
    dl_matrix3du_t *aligned_face = NULL;
    int matched_id = 0;

    aligned_face = dl_matrix3du_alloc(1, FACE_WIDTH, FACE_HEIGHT, 3);
    if(!aligned_face){
        Serial.println("Could not allocate face recognition buffer");
        return matched_id;
    }
    if (align_face(net_boxes, image_matrix, aligned_face) == ESP_OK){
        if (is_enrolling == 1){
            int8_t left_sample_face = enroll_face(&id_list, aligned_face);

            if(left_sample_face == (ENROLL_CONFIRM_TIMES - 1)){
                Serial.printf("Enrolling Face ID: %d\n", id_list.tail);
            }
            Serial.printf("Enrolling Face ID: %d sample %d\n", id_list.tail, ENROLL_CONFIRM_TIMES - left_sample_face);
            // rgb_printf(image_matrix, FACE_COLOR_CYAN, "ID[%u] Sample[%u]", id_list.tail, ENROLL_CONFIRM_TIMES - left_sample_face);
            if (left_sample_face == 0){
                is_enrolling = 0;
                Serial.printf("Enrolled Face ID: %d\n", id_list.tail);
            }
        } else {
            matched_id = recognize_face(&id_list, aligned_face);
            if (matched_id >= 0) {
                Serial.printf("Match Face ID: %u\n", matched_id);
                //rgb_printf(image_matrix, FACE_COLOR_GREEN, "Hello Subject %u", matched_id);
            } else {
                Serial.println("No Match Found");
                //rgb_print(image_matrix, FACE_COLOR_RED, "Intruder Alert!");
                matched_id = -1;
            }
        }
    } else {
        Serial.println("Face Not Aligned");
        //rgb_print(image_matrix, FACE_COLOR_YELLOW, "Human Detected");
    }

    dl_matrix3du_free(aligned_face);
    return matched_id;
}

camera_err shoot() {
    camera_err res = CE_OK;
    camera_fb_t * fb = NULL;
    dl_matrix3du_t *image_matrix = NULL;
    box_array_t *net_boxes = NULL;
    int face_id = 0;
    //size_t _jpg_buf_len = 0;
    //uint8_t * _jpg_buf = NULL;

    fb = esp_camera_fb_get();
    if (!fb) {
        Serial.println("Camera capture failed");
        return CE_FBALLOC;
    }

    if (fb->width > 400) {
        Serial.println("Framebuffer > 400 px, unable to be used for face detection");
        esp_camera_fb_return(fb);
        fb = NULL;
        return CE_TOOLARGE;
    }

    // 3d matrix for face detection
    Serial.print("pixformat: ");
    Serial.print(fb->format);
    Serial.print(" ");

    image_matrix = dl_matrix3du_alloc(1, fb->width, fb->height, 3);
    if (!image_matrix) {
        Serial.println("dl_matrix3du_alloc failed");
        dl_matrix3du_free(image_matrix);
        esp_camera_fb_return(fb);
        fb = NULL;
        return CE_MATRIX;
    }

    if(!fmt2rgb888(fb->buf, fb->len, fb->format, image_matrix->item)){
        Serial.println("fmt2rgb888 failed");
        dl_matrix3du_free(image_matrix);
        esp_camera_fb_return(fb);
        fb = NULL;
        res = CE_RGB888;
    }

    // detect face
    net_boxes = face_detect(image_matrix, &mtmn_config);
    if(net_boxes){
        face_id = run_face_recognition(image_matrix, net_boxes);
        free(net_boxes->score);
        free(net_boxes->box);
        free(net_boxes->landmark);
        free(net_boxes);

        Serial.print("face_id: ");
        Serial.println(face_id);

        if (face_id == -1) {
            esp_camera_fb_return(fb);
            fb = NULL;
            dl_matrix3du_free(image_matrix);

            return CE_UNKOWN_FACE;
        }

    } else {
        Serial.println("Failed to detect face");
        dl_matrix3du_free(image_matrix);
        esp_camera_fb_return(fb);
        fb = NULL;
        return CE_NETBOXES;
    }

    esp_camera_fb_return(fb);
    fb = NULL;
    dl_matrix3du_free(image_matrix);

    return res;
}