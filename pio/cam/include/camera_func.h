enum camera_err {
    CE_OK = 0,
    CE_FBALLOC,
    CE_TOOLARGE,
    CE_MATRIX,
    CE_RGB888,
    CE_NETBOXES,
    CE_FMT2JPG,
    CE_UNKOWN_FACE
};

camera_err shoot();
void camera_init();