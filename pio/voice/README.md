# Audio Information

## ESP32

- https://www.homemade-circuits.com/small-amplifier-circuit/
- https://nodemcu.readthedocs.io/en/release/modules/pcm/
- https://docs.platformio.org/en/latest/platforms/espressif32.html#uploading-files-to-file-system-spiffs

Sigma-Delta hardware
```
GPIO +--[270R]--+-----+---||-----> AMP
                |     |  10u
      33n-68n  ===  [150R]
                +--+--+
                   |
                  GND
```

```
                            2N3904 (NPN)
                            +---------+
                            |         |     +-|
                            | E  B  C |    / S|
                            +-|--|--|-+    | P|
                              |  |  +------+ E|
                              |  |         | A|
ESP8266-GND ------------------+  |  +------+ K| 
                                 |  |      | E|
ESP8266-I2SOUT (Rx) -----/\/\/\--+  |      \ R|
                                    |       +-|
USB 5V -----------------------------+

You may also want to add a 220uF cap from USB5V to GND 
just to help filter out any voltage drop during 
high volume playback.
```

## Walkman Pre-Amp
![Schema](../documentation/walkman_preamp.png)

### Parts List

<img style="float: right;" src="../documentation/bc547b-pinout.png" alt="Pins BC574">

- R1 = 22K
- R2 = 220 ohms
- R3 =100k
- R4 = 4K7
- R5 = 1K
- C1 = 1uF/25V
- C2 = 10uF/25V
- T1/T2 = BC547

