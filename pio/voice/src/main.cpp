#include <Arduino.h>
#ifdef ESP32
    #include <WiFi.h>
#else
    #include <ESP8266WiFi.h>
#endif

/**
 * Pins esp32 <-> MAX98357A 
 * 
 * 26 <-> BLCK
 * 25 <-> LRC
 * 22 <-> DIN
 * 
 * NOTE: provide minimum 650mA (better 850mA) to MAX98357A!
 */
#define I2S_BLCK 26
#define I2S_LRC  25
#define I2S_DOUT 22

// dependencies
#include <SD.h>
#include <HTTPClient.h>
#include "SPIFFS.h"

// audio libs https://github.com/earlephilhower/ESP8266Audio
#include "AudioFileSourcePROGMEM.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"


// I2S pin layout: https://diyi0t.com/i2s-sound-tutorial-for-esp32/
#include "audio.h"

AudioGeneratorMP3 *wav;
AudioFileSourcePROGMEM *file;
AudioOutputI2S *out;

void setup() {
  
  WiFi.mode(WIFI_OFF); 
  Serial.begin(115200);
  delay(1000);
  Serial.printf("WAV start\n");

  audioLogger = &Serial;
  file = new AudioFileSourcePROGMEM(AUDIO_0, AUDIO_0_len);
  out = new AudioOutputI2S();
  wav = new AudioGeneratorMP3();

  out->SetGain(1);
  out->SetPinout(I2S_BLCK, I2S_LRC, I2S_DOUT);
}

int i = 1;

void loop() {

  if (wav->isRunning()) {
    if (!wav->loop()) wav->stop();
  } else {

    if (i == audio_files_len)
      i = 1;
    
    Serial.print("File ");
    Serial.println(i);
    file = new AudioFileSourcePROGMEM(AUDIO[i], AUDIO_len[i]);
    //out = new AudioOutputI2SNoDAC();
    out = new AudioOutputI2S();
    wav = new AudioGeneratorMP3();
    wav->begin(file, out);
    i++;
  }

  delay(50);

}