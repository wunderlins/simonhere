#ifndef SAMPLES_H
#define SAMPLES_H

//#include "samples_data.h"

// number of audio smples
const int audio_files_len = 23;

// file names for better readability. use together with
// AUDIO[], i.e. AUDIO[HELLO] instead of AUDIO[3].
enum AUDIO_FILES {
	NONE = 0,
	SIMON = 1,
	WELCOME = 2,
	HELLO = 3,
	HELLO_MASTER = 4,
	MASTER = 5,
	GOOD_BYE = 6,
	NICE_DAY = 7,
	TIME_4_CURRY = 8,
	TIME_4_BREAK = 9,
	MEETING = 10,
	GO_GOME = 11,
	TGIF = 12,
	MINORITIES = 13,
	FAT_WOMEN = 14,
	SC_LOCK = 15,
	SC_UNLOCK = 16,
	TIME_SYNC = 17,
	INIT = 18,
	WIFI = 19,
	CONNECTED = 20,
	TIMEOUT = 21,
	HELLO_MASTER_SIMON = 22,
};


#endif // SAMPLES_H
