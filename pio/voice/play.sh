#!/usr/bin/env bash

if [[ "1$(uname)" != "1Darwin" ]]; then
	echo "Must be run on OSX"
	exit 1;
fi

files=$(ls audio| sort -g)

for f in $files; do
	id=$(echo "$f" | sed -e 's,.aiff,,')
	say -v Oliver $id
	afplay audio/$f
done
