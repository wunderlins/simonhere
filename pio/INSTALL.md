# PlatformIO Installation

- website: https://platformio.org/install/cli
- https://docs.platformio.org/en/latest/core/installation.html#installation-methods
- wget https://raw.githubusercontent.com/platformio/platformio-core-installer/master/get-platformio.py -O get-platformio.py && python3 get-platformio.py
 

# PlatformIO CLI

- Webserver: `pio home`, startet den webserver
- Boards: `pio boards digispark`, sucht nach digispark boards
- Initialize Project: `pio project init --board wemos_d1_mini32 --board digispark-tiny`, generiert ein projekt für wemos esp32 und digispark ATTiny85
- `platform.ini` serial speed: `monitor_speed = 115200`

# Basic Commands

`include` - Put project header files here<br>
`lib` - Put here project specific (private) libraries<br>
`src` - Put project source files here<br>
`platformio.ini` - Project Configuration File

Project has been successfully initialized! Useful commands:<br>
`pio run` - process/build project from the current directory<br>
`pio run --target upload` or `pio run -t upload` - upload firmware to a target<br>
`pio run --target clean` - clean project (remove compiled files)<br>
`pio run --help` - additional information
