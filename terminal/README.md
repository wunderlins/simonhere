# SimonHERE configuration GUI

## Build

You need to have Qt5.12-dev installed. 

	mkdir ../build
	cd ../build
	qmake ../terminal/terminal.pro 'CONFIG+=release'
	make -j4 # or for whatever many cores you build on

## Package

### Linux

This utility will copy all dependencies into one tar file. Do a release build first.

	./tools/package_linux.sh



