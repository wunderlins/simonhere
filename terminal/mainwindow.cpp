/****************************************************************************
**
** Copyright (C) 2012 Denis Shienkov <denis.shienkov@gmail.com>
** Copyright (C) 2012 Laszlo Papp <lpapp@kde.org>
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtSerialPort module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "console.h"
#include "settingsdialog.h"
#include "state.h"
#include <QLabel>
#include <QMessageBox>
#include <QDebug>
#include <QStringListModel>
#include <QDateTime>

//! [0]
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow),
    m_status(new QLabel),
    m_console(new Console),
    m_settings(new SettingsDialog),
//! [1]
    m_serial(new QSerialPort(this)),
//! [1]
    m_state(new state(this)),
    model(new QStringListModel(this))
{
//! [0]
    m_ui->setupUi(this);
    m_console->setEnabled(false);
    //setCentralWidget(m_console);
    m_ui->tabWidget->insertTab(2, m_console, "Console");

    m_ui->actionConnect->setEnabled(true);
    m_ui->actionDisconnect->setEnabled(false);
    m_ui->actionQuit->setEnabled(true);
    m_ui->actionConfigure->setEnabled(true);

    m_ui->statusBar->addWidget(m_status);

    model->setStringList(macs);
    m_ui->mac_history->setModel(model);

    m_ui->mainToolBar->hide();

    initActionsConnections();

    connect(m_serial, &QSerialPort::errorOccurred, this, &MainWindow::handleError);

//! [2]
    connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::readData);
//! [2]
    connect(m_console, &Console::getData, this, &MainWindow::writeData);
//! [3]
}
//! [3]

MainWindow::~MainWindow()
{
    delete m_settings;
    delete m_ui;
}

//! [4]
void MainWindow::openSerialPort()
{
    const SettingsDialog::Settings p = m_settings->settings();
    m_serial->setPortName(p.name);
    m_serial->setBaudRate(p.baudRate);
    m_serial->setDataBits(p.dataBits);
    m_serial->setParity(p.parity);
    m_serial->setStopBits(p.stopBits);
    m_serial->setFlowControl(p.flowControl);
    if (m_serial->open(QIODevice::ReadWrite)) {
        m_console->setEnabled(true);
        m_console->setLocalEchoEnabled(p.localEchoEnabled);
        m_ui->actionConnect->setEnabled(false);
        m_ui->actionDisconnect->setEnabled(true);
        m_ui->actionConfigure->setEnabled(false);
        showStatusMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                          .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                          .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));
        // activate status ui
        m_ui->tab_status->setEnabled(true);
    } else {
        QMessageBox::critical(this, tr("Error"), m_serial->errorString());

        showStatusMessage(tr("Open error"));
    }
}
//! [4]

//! [5]
void MainWindow::closeSerialPort()
{
    if (m_serial->isOpen())
        m_serial->close();
    m_console->setEnabled(false);
    m_ui->actionConnect->setEnabled(true);
    m_ui->actionDisconnect->setEnabled(false);
    m_ui->actionConfigure->setEnabled(true);

    // disable ui
    m_ui->tab_status->setEnabled(false);
    m_ui->tab_config->setEnabled(false);

    showStatusMessage(tr("Disconnected"));
}
//! [5]

void MainWindow::about()
{
    QMessageBox::about(this, tr("SimonHERE Configuration Utility"),
                       tr("This Utility is the frontend to monitor and "
                          "configure the SimonHERE device."));
}

//! [6]
void MainWindow::writeData(const QByteArray &data)
{
    m_serial->write(data);
    // qDebug() << data;
}
//! [6]

//! [7]
void MainWindow::readData()
{
    const QByteArray data = m_serial->readAll();
    m_console->putData(data);
    //qDebug() << data;
    m_state->parse_serial_input(&data);
}
//! [7]

//! [8]
void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), m_serial->errorString());
        closeSerialPort();
    }
}
//! [8]

void MainWindow::initActionsConnections()
{
    connect(m_ui->actionConnect, &QAction::triggered, this, &MainWindow::openSerialPort);
    connect(m_ui->actionDisconnect, &QAction::triggered, this, &MainWindow::closeSerialPort);
    connect(m_ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
    connect(m_ui->actionConfigure, &QAction::triggered, m_settings, &SettingsDialog::show);
    connect(m_ui->actionClear, &QAction::triggered, m_console, &Console::clear);
    connect(m_ui->actionAbout, &QAction::triggered, this, &MainWindow::about);
    connect(m_ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);

    // console parser, status updates
    connect(m_state, &state::stateChanged, this, &MainWindow::stateChanged);
    connect(m_state, &state::configChanged, this, &MainWindow::configUpdated);
    connect(m_state, &state::showStatusMessage, this, &MainWindow::showStatusMessage);
}

void MainWindow::showStatusMessage(const QString &message)
{
    m_status->setText(message);
}

void MainWindow::macChanged(QString part) {
    // user input is validated in the gui with a hex mask.
    // every element shall send a valid hex pair, then we save
    //qDebug() << "macChanged(QString): " << part << ", length: " << part.length();

    /*
    qDebug() << "Acceptable input on mac addr: "
             << m_ui->config_mac_0->hasAcceptableInput() << " "
             << m_ui->config_mac_1->hasAcceptableInput() << " "
             << m_ui->config_mac_2->hasAcceptableInput() << " "
             << m_ui->config_mac_3->hasAcceptableInput() << " "
             << m_ui->config_mac_4->hasAcceptableInput() << " "
             << m_ui->config_mac_5->hasAcceptableInput();
    */

    // check if all mac parts validate
    if (!m_ui->config_mac_0->hasAcceptableInput() ||
        !m_ui->config_mac_1->hasAcceptableInput() ||
        !m_ui->config_mac_2->hasAcceptableInput() ||
        !m_ui->config_mac_3->hasAcceptableInput() ||
        !m_ui->config_mac_4->hasAcceptableInput() ||
        !m_ui->config_mac_5->hasAcceptableInput()) {

        // tell the user that there is something wrong
        showStatusMessage("Invalid MAC Address");
        return;
    }

    // mac seems to be valid, send it and issue a read command
    QString cmd = "set mac " +
                  m_ui->config_mac_0->text() + ":" +
                  m_ui->config_mac_1->text() + ":" +
                  m_ui->config_mac_2->text() + ":" +
                  m_ui->config_mac_3->text() + ":" +
                  m_ui->config_mac_4->text() + ":" +
                  m_ui->config_mac_5->text() + "\nget json\n";

    writeData(cmd.toUtf8());
    showStatusMessage("MAC Address saved");
}

void MainWindow::stateChanged(current_state_t state) {
    /*
    qDebug() << "MainWindow::stateChanged(): " << state.wifi_rssi
             << " | " << state.wifi_channel << " | " << state.wifi_last_mac
             << " | " << state.wifi_last_seen_mode;
    */
    int min = (state.wifi_rssi > -50) ? -50 : state.wifi_rssi;
    m_ui->prog_rssi->setValue(min);
    m_ui->prog_last_seen->setValue(state.wifi_last_seen_current);
    m_ui->wifi_channel->setNum(state.wifi_channel);

    // last seen mode
    if (state.wifi_last_seen_mode == 0) {
        m_ui->wifi_state_0->toggle();
    } else if (state.wifi_last_seen_mode == 1) {
        m_ui->wifi_state_1->toggle();
    } else {
        m_ui->wifi_state_2->toggle();
    }

    if (state.event == 1) {
        if (macs.length() > 500) {
            macs.removeLast();
        }

        QDateTime current(QDateTime::currentDateTime());
        macs.prepend(current.toString("HH:mm:ss") + ", " +
                     state.wifi_last_mac + ", " +
                     QString::number(state.wifi_rssi) + ", " +
                     QString::number(state.wifi_channel));
        model->setStringList(macs);
    }

}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    //qDebug() << "Tab activated: " << index;

    // if index is 1, then load json configuration
    if (index != 1)
        return;

    // make sure we have an open connection
    if (m_serial->isOpen() == false)
        return;

    // send "get json\n"
    QByteArray cmd = {"get json\r"};
    writeData(cmd);

    // let the parser do the rest
    // -> will emit the signal state::configChanged()
    // -> which should be connected to MainWindow::configUpdated()
}

void MainWindow::configUpdated(airspy_config_t lconfig) {
    //qDebug() << "MainWindow::configUpdated(config)";
    //qDebug() << "rssi: " << QString(lconfig.min_rssi);

    // update all fields
    m_ui->config_min_rssi->setText(QString::number(lconfig.min_rssi));
    m_ui->config_scan_delay->setText(QString::number(lconfig.scan_delay));
    m_ui->config_wifi_chan_max->setText(QString::number(lconfig.wifi_chan_max));
    m_ui->config_wifi_chan_switch_int->setText(QString::number(lconfig.wifi_chan_switch_int));
    m_ui->config_max_last_seen->setText(QString::number(lconfig.max_last_seen));

    // update mac address
    QString digit;
    /*
    qDebug() << "mac: " << lconfig.mac[0] << " "
                        << lconfig.mac[1] << " "
                        << lconfig.mac[2] << " "
                        << lconfig.mac[3] << " "
                        << lconfig.mac[4] << " "
                        << lconfig.mac[5];
    */

    digit = QString("%1").arg(lconfig.mac[0], 2, 16, QChar('0'));
    m_ui->config_mac_0->setText(digit.toUpper());
    digit = QString("%1").arg(lconfig.mac[1], 2, 16, QChar('0'));
    m_ui->config_mac_1->setText(digit.toUpper());
    digit = QString("%1").arg(lconfig.mac[2], 2, 16, QChar('0'));
    m_ui->config_mac_2->setText(digit.toUpper());
    digit = QString("%1").arg(lconfig.mac[3], 2, 16, QChar('0'));
    m_ui->config_mac_3->setText(digit.toUpper());
    digit = QString("%1").arg(lconfig.mac[4], 2, 16, QChar('0'));
    m_ui->config_mac_4->setText(digit.toUpper());
    digit = QString("%1").arg(lconfig.mac[5], 2, 16, QChar('0'));
    m_ui->config_mac_5->setText(digit.toUpper());

    // enable ui
    m_ui->tab_config->setEnabled(true);
}


void MainWindow::on_config_min_rssi_textEdited(const QString &arg1)
{
    qDebug() << "rssi new value: " << m_ui->config_min_rssi->text().toInt();

    QString cmd = "set rssi " + m_ui->config_min_rssi->text() + "\n";
    writeData(cmd.toUtf8());
    showStatusMessage("RSSI saved");
}

void MainWindow::on_config_scan_delay_textEdited(const QString &arg1)
{
    qDebug() << "main_loop_delay new value: " << m_ui->config_scan_delay->text().toInt();

    QString cmd = "set main_loop_delay " + m_ui->config_scan_delay->text() + "\n";
    writeData(cmd.toUtf8());
    showStatusMessage("main_loop_delay saved");
}

void MainWindow::on_config_wifi_chan_max_textEdited(const QString &arg1)
{
    qDebug() << "max_chan new value: " << m_ui->config_wifi_chan_max->text().toInt();

    QString cmd = "set max_chan " + m_ui->config_wifi_chan_max->text() + "\n";
    writeData(cmd.toUtf8());
    showStatusMessage("max_chan saved");
}

void MainWindow::on_config_wifi_chan_switch_int_textEdited(const QString &arg1)
{
    qDebug() << "rssi new value: " << m_ui->config_wifi_chan_switch_int->text().toInt();

    QString cmd = "set chan_interval " + m_ui->config_wifi_chan_switch_int->text() + "\n";
    writeData(cmd.toUtf8());
    showStatusMessage("Channel Interval saved");
}

void MainWindow::on_config_max_last_seen_textEdited(const QString &arg1)
{
    qDebug() << "last_seen new value: " << m_ui->config_max_last_seen->text().toInt();

    QString cmd = "set last_seen " + m_ui->config_max_last_seen->text() + "\n";
    writeData(cmd.toUtf8());
    showStatusMessage("max_last_seen saved");
}
