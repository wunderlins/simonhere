#ifndef STATE_H
#define STATE_H

#include <QObject>

enum state_change_event_t {
    TIMER = 0,
    RSSI = 1,
    CONFIG = 2
};

enum airspy_state_mode_t {
    SCANNING = 0,
    CONSOLE  = 1
};

typedef enum {
    CONNECTED = 0,
    UNKNOWN,
    TIMEOUT
} wifi_state_t;

struct airspy_state_t {
    airspy_state_mode_t mode = SCANNING;
};


// default configuration, if EEPROM is empty
struct airspy_config_t {
    uint8_t  mac[6] = {255, 255, 255, 255, 255, 255};
    uint16_t scan_delay = 500;
    uint8_t min_rssi = 80;
    uint8_t wifi_chan_max = 13;
    uint16_t wifi_chan_switch_int = 250;
    uint16_t max_last_seen = 180;
};

struct current_state_t {
    int wifi_rssi = 0;
    int wifi_last_seen = 0;
    int wifi_last_seen_current = 0;
    int wifi_last_seen_mode = 1;
    int wifi_channel = 0;
    QString wifi_last_mac = "";
    state_change_event_t event = TIMER;
};

class state : public QObject
{
    Q_OBJECT
    //Q_PROPERTY(current_state_t current_state READ getState WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(current_state_t current_state READ getState WRITE setState NOTIFY stateChanged)

public:
    explicit state(QObject *parent = nullptr);
    bool parse_serial_input(const QByteArray *line);

    current_state_t getState();
    void setState(current_state_t current_state);

    const QString cmd_stqart = "$$$";
    const QString cmd_end    = "+++";
    bool parse_line(const QByteArray *line);

signals:
    // wifi signals
    void stateChanged(current_state_t current_state);
    void configChanged(airspy_config_t config);
    void showStatusMessage(const QString &message);

private:

    current_state_t *current_state;
    QByteArray input_buffer = {};

    struct airspy_state_t console_state;
    struct airspy_config_t *config;

};

#endif // STATE_H
