
#!/usr/bin/env bash

build_dir="../build-terminal-Desktop_Qt_5_12_7_clang_64bit-Release"
qt_dir="/opt/Qt/5.12.7/clang_64/bin"
export PATH="$PATH":$qt_dir

macdeployqt $build_dir/terminal.app -always-overwrite # -dmg
mkdir -p ../build/osx
rm -R ../build/osx/*
cp -Rp $build_dir/terminal.app ../build/osx
cp ../release/drivers/SiLabsUSBDriverDisk.dmg ../build/osx
#cp $build_dir/terminal.dmg ../release/osx.dmg

# hdiutil create -volname WhatYouWantTheDiskToBeNamed -srcfolder /path/to/the/folder/you/want/to/create -ov -format UDZO name.dmg
rm ../release/osx.dmg 2>/dev/null
hdiutil create -volname "SimonHERE Terminal" -srcfolder ../build/osx \
	-ov -format UDZO ../release/osx.dmg
