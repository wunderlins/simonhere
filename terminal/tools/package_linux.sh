#!/usr/bin/env bash
function filename () {
    ldd $1 | grep /Qt/ | cut -d' ' -f3;
}

# get build directory
build_dir=$(grep target.path terminal.pro | sed -e 's/.*= *//; s/\n//; s/\r//')
echo "build_dir: '$build_dir'"

# find dependiencies
filename $build_dir/terminal > dependencies.txt
for f in `filename $build_dir/terminal`; do
    filename $f;
done >> dependencies.txt
dependencies=$(cat dependencies.txt | sort | uniq)
rm dependencies.txt

dst_dir=../release/`uname -s`/`uname -m`
echo "dst_dir: $dst_dir"
mkdir -p $dst_dir
cp $build_dir/terminal $dependencies $dst_dir
cp images/logo.png $dst_dir/icon.png

dst_file="../release/`uname -s`-`uname -m`.tar.gz"
echo "dst_file: $dst_file"
rm $dst_file 2>/dev/null
files=$(find $dst_dir -type f -exec basename {} \;)
echo $files

echo "packing archive ... "
tar -C "$dst_dir" -cvzf $dst_file $files
