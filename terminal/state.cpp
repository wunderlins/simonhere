#include "state.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>

state::state(QObject *parent) :
    QObject(parent),
    current_state(new current_state_t),
    config(new airspy_config_t)
{

}

bool state::parse_serial_input(const QByteArray *string) {

    // check if the last character of the line is a \r or \n
    // if the whoe string is not terminated by a newline, the store it in
    // a buffer and for more input until we find a new line
    input_buffer.append(*string);
    QString sline = input_buffer.data();

    QStringList lines = sline.split("\n");
    auto count = lines.length();
    bool keep_last = false;
    if (string->at(string->length()-1) == '\n' ||
        string->at(string->length()-1) == '\r') {
        keep_last = true;
        count--; // skip last element
    }

    for(int i = 0; i < count; i++) {
        const QByteArray line = lines.at(i).toUtf8();
        parse_line(&line);
    }

    // update input buffer
    if (keep_last)
        input_buffer = lines.at(count).toUtf8();
    else
        input_buffer = "";

    return true;
}

bool state::parse_line(const QByteArray *line) {
    QString sline = line->data();
    qDebug() << "line: " << sline;

    // check if we are scanning or if we are in console mode
    if (sline.indexOf('>', 0) == 0) {
        console_state.mode = CONSOLE;
    } else {
        console_state.mode = SCANNING;
    }

    // parse json data
    if (sline.indexOf('{', 0) == 0) {
        QString line = sline.split("\n")[0].simplified();
        qDebug() << "json: " << line.simplified();
        QJsonDocument d = QJsonDocument::fromJson(line.toUtf8());
        qWarning() << d.isNull(); // <- print false :)

        // parsing of result failed
        if (d.isNull()) {
            emit showStatusMessage("Failed to parse configuration");
            return false;
         }

        // update our central config structure
        QJsonObject cfg = d.object();
        /*
        qWarning() << cfg.value(QString("mac"));
        qWarning() << cfg.value(QString("min_rssi"));
        qWarning() << cfg.value(QString("scan_delay"));
        qWarning() << cfg.value(QString("wifi_chan_max"));
        qWarning() << cfg.value(QString("wifi_chan_switch_int"));
        qWarning() << cfg.value(QString("max_last_seen"));
        */

        config->min_rssi = cfg.value(QString("min_rssi")).toInt();
        config->scan_delay = cfg.value(QString("scan_delay")).toInt();
        config->wifi_chan_max = cfg.value(QString("wifi_chan_max")).toInt();
        config->wifi_chan_switch_int = cfg.value(QString("wifi_chan_switch_int")).toInt();
        config->max_last_seen = cfg.value(QString("max_last_seen")).toInt();

        // parse mac ADDR
        QString mac = cfg.value(QString("mac")).toString();
        QStringList mac_parts = mac.split(':');
        bool ok;
        for (int i = 0; i<mac_parts.size(); i++) {
            //qDebug() << "mac[" << i << "]: " << mac_parts.at(i);
            config->mac[i] = mac_parts.at(i).toUInt(&ok, 16);
        }

        // now update the user interface
        current_state->event = CONFIG;
        //emit showStatusMessage("Parsed configuration from uController");
        emit configChanged(*config);
    }

    // check if the first 4 bytes include a prefix we use for state
    if (sline.startsWith("WIFI state")) {

        // WIFI state: 0, since sec: 28/600

        // parse timeout
        QString timer = sline.mid(26).trimmed();
        auto sep = timer.indexOf('/', 0);
        current_state->wifi_last_seen_current = timer.mid(0, sep).toUInt();
        current_state->wifi_last_seen         = timer.mid(sep+1).toUInt();

        // parse last seen state
        current_state->wifi_last_seen_mode = timer.mid(12, 1).toUInt();

        /*
        qDebug() << current_state.wifi_last_seen_current << " / " << current_state.wifi_last_seen
                 << " | " << current_state.wifi_last_seen_mode;
        */

        current_state->event = TIMER;
        emit stateChanged(*current_state);
        return true;
    }

    // check channel and RSSI
    if (sline.startsWith("WIFI: ")) {
        QString part = sline.mid(6).trimmed();
        auto parts = part.split(", ");

        // ("26s", "CHAN=11", "RSSI=-67", "a4:50:46:c5:ec:c9 ->  b0:be:76:6d:cc:cb")
        // 0: timeout since
        // 1: Channel
        // 2: rssi
        // 3: my mac -> remote mac
        current_state->wifi_channel = parts[1].mid(5).toUInt();
        current_state->wifi_rssi    = parts[2].mid(5).toInt();

        // remote mac addr
        current_state->wifi_last_mac = parts[3].mid(22);

        /*
        qDebug() << parts;
        qDebug() << current_state->wifi_channel << " | "
                 << current_state->wifi_rssi << " | "
                 << current_state->wifi_last_mac;
        */
        current_state->event = RSSI;
        emit stateChanged(*current_state);
        return true;
    }

    return false;
}

current_state_t state::getState() {
    return *current_state;
}
void state::setState(current_state_t cs) {
    current_state = &cs;
}


