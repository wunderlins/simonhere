QT += widgets serialport
requires(qtConfig(combobox))

# disable debug and console output on release builds
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
CONFIG(release, debug|release):DEFINES += QT_NO_WARNING_OUTPUT

TARGET = terminal
TEMPLATE = app

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    settingsdialog.cpp \
    console.cpp \
    state.cpp

HEADERS += \
    mainwindow.h \
    settingsdialog.h \
    console.h \
    state.h

FORMS += \
    mainwindow.ui \
    settingsdialog.ui

RESOURCES += \
    terminal.qrc

target.path = ../build
INSTALLS += target
