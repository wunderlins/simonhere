#!/usr/bin/env runc

#include <stdio.h>
#include <stdint.h>

void shift() {
    static uint16_t i = 1;
    
    i = i << 1;
    printf("value: %d\n", i);
} 

int main() {
    
    for (int c=0; c<8; c++)
        shift();
    
    return 0;
}
